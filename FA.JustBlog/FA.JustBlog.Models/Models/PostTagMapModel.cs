﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Models.Models
{
    public class PostTagMapModel
    {
        public Guid TagId { get; set; }
        public Guid PostId { get; set; }
    }
}
