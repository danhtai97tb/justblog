﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Models.Models.Post
{
    public class EditPostViewModel
    {
        public Guid Id { get; set; }
        [Required]
        [StringLength(200)]
        public string Title { get; set; }

        [Required]
        [StringLength(1024)]
        public string ShortDescription { get; set; }

        [Required]
        [StringLength(200)]
        public string PostContent { get; set; }

        [Required]
        [StringLength(200)]
        public string UrlSlug { get; set; }

        public bool Published { get; set; }
        public virtual Guid CategoryId { get; set; }
        //        public Guid TagId { get; set; }

        [Required]
        [StringLength(200)]
        public string TagName { get; set; }
        public IEnumerable<TagModel>? TagList { get; set; }
        public IEnumerable<CommentModel>? CommentList { get; set; }
    }
}
