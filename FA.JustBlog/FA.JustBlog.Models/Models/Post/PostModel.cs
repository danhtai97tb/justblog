﻿using FA.JustBlog.Entity.Entity;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FA.JustBlog.Models.Models.Post
{
    public class PostModel
    {
        public Guid Id { get; set; }

        [StringLength(255, MinimumLength = 1, ErrorMessage = "The {0} must between {2} and {1} character.")]
        public string Title { get; set; }

        [StringLength(255, MinimumLength = 1, ErrorMessage = "The {0} must between {2} and {1} character.")]
        public string ShortDescription { get; set; }

        [Required(ErrorMessage = "The {0} is required.")]
        [Display(Name = "Post Content")]
        public string PostContent { get; set; }

        [Required(ErrorMessage = "The {0} is required.")]
        [Display(Name = "Url Slug")]
        public string UrlSlug { get; set; }

        [Required(ErrorMessage = "The {0} is required.")]
        public bool Published { get; set; }

        [Required(ErrorMessage = "The {0} is required.")]
        public DateTime PostedOn { get; set; }

        [Required(ErrorMessage = "The {0} is required.")]
        public DateTime Modified { get; set; }
        public virtual Guid CategoryId { get; set; }
        public virtual Category Category { get; set; }

        [Required(ErrorMessage = "The {0} is required.")]
        public int ViewCount { get; set; }

        [Required(ErrorMessage = "The {0} is required.")]
        public int RateCount { get; set; }

        public int TotalRate { get; set; }

        public string CategoryName { get; set; }

        public Guid TagId { get; set; }

        [StringLength(255, MinimumLength = 1, ErrorMessage = "The {0} must between {2} and {1} character.")]
        public string TagName { get; set; }

        public decimal Rate { get => TotalRate / (RateCount == 0 ? 1 : RateCount); }

        public IEnumerable<TagModel> TagList { get; set; }
        public ICollection<CommentModel> CommentList { get; set; }


    }
}
