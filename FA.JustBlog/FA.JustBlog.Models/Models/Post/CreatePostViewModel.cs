﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Models.Models.Post
{
    public class CreatePostViewModel
    { 
        public Guid Id { get; set; }
        [StringLength(255, MinimumLength = 2, ErrorMessage = "The {0} must between {2} and {1} character.")]
        [Required(ErrorMessage = "The {0} is required.")]
        [Display(Name = "Title")]
        public string Title { get; set; }

        [StringLength(255, MinimumLength = 2, ErrorMessage = "The {0} must between {2} and {1} character.")]
        [Required(ErrorMessage = "The {0} is required.")]
        [Display(Name = "ShortDescription")]
        public string ShortDescription { get; set; }

        [StringLength(255, MinimumLength = 2, ErrorMessage = "The {0} must between {2} and {1} character.")]
        [Required(ErrorMessage = "The {0} is required.")]
        [Display(Name = "PostContent")]
        public string PostContent { get; set; }

        
        [Required(ErrorMessage = "The {0} is required.")]
        [Display(Name = "PostContent")]
        public DateTime PostedOn { get; set; }

        [Required(ErrorMessage = "The {0} is required.")]
        [Display(Name = "Url Slug")]
        public string? UrlSlug { get; set; }

        [Required(ErrorMessage = "The {0} is required.")]
        public bool Published { get; set; }
        public virtual Guid CategoryId { get; set; }
        //        public Guid TagId { get; set; }


        [StringLength(255, MinimumLength = 2, ErrorMessage = "The {0} must between {2} and {1} character.")]
        [Required(ErrorMessage = "The {0} is required.")]
        [Display(Name = "Tag Name")]
        public string TagName { get; set; }
        public IEnumerable<TagModel>? TagList { get; set; }

    }
}
