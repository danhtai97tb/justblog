﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace FA.JustBlog.Models.Models
{
    public class CommentModel
    {
        public Guid Id { get; set; }

        [StringLength(255, MinimumLength = 2, ErrorMessage = "The {0} must between {2} and {1} character.")]
        [Required(ErrorMessage = "The {0} is required.")]
        [Display(Name = "Comment Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "The {0} is required.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "The {0} is required.")]
        public string CommentHeader { get; set; }

        [Required(ErrorMessage = "The {0} is required.")]
        public string CommentText { get; set; }

        [Required(ErrorMessage = "The {0} is required.")]
        public DateTimeOffset CommentTime { get; set; }

        public virtual Guid PostId { get; set; }

        [Required(ErrorMessage = "The {0} is required.")]
        public string PostTitle { get; set; }

       
    }
}
