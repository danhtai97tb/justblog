﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Models.AppRole
{
    public class CreateRoleViewModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
