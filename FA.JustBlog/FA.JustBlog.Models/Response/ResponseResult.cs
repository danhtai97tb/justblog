﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Models.Response
{
    public class ResponseResult<TData>
    {
        /// <summary>
        /// Create a response with sucscess result
        /// </summary>
        public ResponseResult()
        {
            this.IsSuccessed = true;
        }
        /// <summary>
        /// Create a response with faile result with errror message
        /// </summary>
        /// <param name="errorMessage"></param>
        public ResponseResult(string Message)
        {
            this.IsSuccessed = false;
            this.Message = Message;
        }
        /// <summary>
        /// State of response
        /// </summary>
        public bool IsSuccessed { get; set; }

        /// <summary>
        /// Error Message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Status code
        /// </summary>
        public int StatusCode { get; set; }

        /// <summary>
        /// Data
        /// </summary>
        public TData Data { get; set; }

        public IEnumerable<TData> DataList { get; set; }
    }

}

