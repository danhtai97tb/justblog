﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FA.JustBlog.Entity.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AppUser",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(100)", nullable: false),
                    UserName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NormalizedUserName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NormalizedEmail = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    PasswordHash = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SecurityStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    TwoFactorEnabled = table.Column<bool>(type: "bit", nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    LockoutEnabled = table.Column<bool>(type: "bit", nullable: false),
                    AccessFailedCount = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppUser", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoleClaims", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NormalizedName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserClaims", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserLogin",
                columns: table => new
                {
                    ProviderKey = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    LoginProvider = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ProviderDisplayName = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserLogin", x => new { x.UserId, x.ProviderKey });
                });

            migrationBuilder.CreateTable(
                name: "UserRole",
                columns: table => new
                {
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    RoleId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRole", x => new { x.UserId, x.RoleId });
                });

            migrationBuilder.CreateTable(
                name: "UserToken",
                columns: table => new
                {
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserToken", x => new { x.UserId, x.LoginProvider });
                });

            migrationBuilder.CreateTable(
                name: "Category",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    UrlSlug = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(1024)", maxLength: 1024, nullable: false),
                    AppUserId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Category", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Category_AppUser_AppUserId",
                        column: x => x.AppUserId,
                        principalTable: "AppUser",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Tag",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    UrlSlug = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Count = table.Column<int>(type: "int", nullable: false),
                    AppUserId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tag", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tag_AppUser_AppUserId",
                        column: x => x.AppUserId,
                        principalTable: "AppUser",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Post",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    ShortDescription = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    PostContent = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UrlSlug = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Published = table.Column<bool>(type: "bit", nullable: false),
                    PostedOn = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Modified = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CategoryId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ViewCount = table.Column<int>(type: "int", nullable: false),
                    RateCount = table.Column<int>(type: "int", nullable: false),
                    TotalRate = table.Column<int>(type: "int", nullable: false),
                    AppUserId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Post", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Post_AppUser_AppUserId",
                        column: x => x.AppUserId,
                        principalTable: "AppUser",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Post_Category_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Comment",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Email = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    CommentHeader = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CommentText = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CommentTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    PostId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Comment_Post_PostId",
                        column: x => x.PostId,
                        principalTable: "Post",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PostTagMap",
                columns: table => new
                {
                    PostId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    TagId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostTagMap", x => new { x.PostId, x.TagId });
                    table.ForeignKey(
                        name: "FK_PostTagMap_Post_PostId",
                        column: x => x.PostId,
                        principalTable: "Post",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PostTagMap_Tag_TagId",
                        column: x => x.TagId,
                        principalTable: "Tag",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "AppUser",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "Name", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { new Guid("c952f359-6b89-4723-9567-ec477aef3009"), 0, "2882ee4c-ba29-4c23-b6e4-908e4d1d37af", null, false, false, null, "Admin", null, null, null, null, false, null, false, null });

            migrationBuilder.InsertData(
                table: "Category",
                columns: new[] { "Id", "AppUserId", "Description", "Name", "UrlSlug" },
                values: new object[,]
                {
                    { new Guid("0a43ec60-ba72-4929-a669-d30d23f00700"), null, "Life Style Blog", "Life Style", "life-style" },
                    { new Guid("1175989d-97f4-4e70-b1fa-2d8e88d905a7"), null, "Tips Blog", "Tips", "tips" },
                    { new Guid("131c273d-bbab-47d3-ac4e-f8cd7f3c25a0"), null, "Travel Blog", "Travel", "travel" },
                    { new Guid("fd6cd3bb-58a3-4541-b419-647d1e1d820c"), null, "Recipe Blog", "Recipe", "recipe" }
                });

            migrationBuilder.InsertData(
                table: "Tag",
                columns: new[] { "Id", "AppUserId", "Count", "Description", "Name", "UrlSlug" },
                values: new object[,]
                {
                    { new Guid("17483cc0-5c58-4144-963d-4023840e801e"), null, 0, "study Tag", "study", "study" },
                    { new Guid("8f116ef5-8ec8-4d97-9a48-4a2fe660a547"), null, 2, "food Tag", "food", "food" },
                    { new Guid("a7be5849-51fa-494f-a38c-78fdc7ea8151"), null, 0, "tips Tag", "tips", "tips" },
                    { new Guid("e5353cd0-50eb-4762-915e-0c91f026c108"), null, 3, "recipe Tag", "recipe", "recipe" },
                    { new Guid("f6597c20-249d-48c0-aba7-4cd21cef8b69"), null, 1, "Travel Tag", "travel", "travel" },
                    { new Guid("f89e3fa9-d563-472a-ae4d-d29ffc53c913"), null, 0, "life style Tag", "life style", "life-style" }
                });

            migrationBuilder.InsertData(
                table: "Post",
                columns: new[] { "Id", "AppUserId", "CategoryId", "Modified", "PostContent", "PostedOn", "Published", "RateCount", "ShortDescription", "Title", "TotalRate", "UrlSlug", "ViewCount" },
                values: new object[,]
                {
                    { new Guid("0f8fad5b-d9cb-469f-a165-70867728950e"), null, new Guid("fd6cd3bb-58a3-4541-b419-647d1e1d820c"), new DateTime(2022, 8, 17, 9, 56, 50, 147, DateTimeKind.Local).AddTicks(7268), "Yes, the traffic is annoying and crazy. I often count my lucky stars for not knowing a lot of dirty language in Vietnamese, because I’d be spouting it a lot. At people that absolutely need to pass me on the left 2 meters before they take a right turn. At people who feel that while driving a motorbike is the perfect time for them to check their Facebook comments. At people who seem to think their horn creates a force field, so they keep it blaring while they speed through tiny openings in traffic with no regard for anyone else or even themselves.", new DateTime(2022, 8, 17, 9, 56, 50, 147, DateTimeKind.Local).AddTicks(7268), true, 13, "Motorbikes in Hanoi. Alright, so it’s been a while since my last (and first and only) post. I could come up with any number of excuses...", "A Tay on Wheels", 31, "a-tay-on-wheels", 125 },
                    { new Guid("465ee23d-c44a-41e4-98a0-5a41754b6607"), null, new Guid("131c273d-bbab-47d3-ac4e-f8cd7f3c25a0"), new DateTime(2022, 8, 17, 9, 56, 50, 147, DateTimeKind.Local).AddTicks(7249), "Wait for the next delayed flight in 3 hours. Wait on the train for 12 hours before arrival. Wait for someone who is never going to text you back. Wait for something that is never going to fall in your mouth. Wait for a story where you could become the main character. Wait for the inevitable conclusion that awaits everyone.", new DateTime(2020, 10, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), true, 10, "Wait for the next delayed flight in 3 hours. Wait on the train for 12 hours before arrival. Wait for someone who is never going to...", "What to do when you have to wait", 46, "what-to-do-when-you-have-to-wait", 112 },
                    { new Guid("7c9e6679-7425-40de-944b-e07fc1f90ae7"), null, new Guid("fd6cd3bb-58a3-4541-b419-647d1e1d820c"), new DateTime(2022, 8, 17, 9, 56, 50, 147, DateTimeKind.Local).AddTicks(7271), "We are naturally drawn to statements that could resonate with us and our place in society. The existential and political angst the artist faces in modern times as they’re conveying ideas, through conventional means of art. The recent Joker movie is a prime example of how a fictional character could be representative and even reflective of certain group(s) of people, in this case, the mentally ill. The character Arthur Fleck (presumably based on respectively the two classic Martin’s characters Travis Bickle and Rupert Pupkin) should give us a clear, albeit twisted image of a modern tragedy: He’s a mentally ill loner who’s also got a medical condition where he can’t control his laugh. He’s misunderstood, isolated and mistreated by society, who harbors dangerous perspectives about life and about himself.", new DateTime(2022, 8, 17, 9, 56, 50, 147, DateTimeKind.Local).AddTicks(7271), true, 13, "It’s extremely difficult to define “art” in modern times. As of recent, postmodernism has taken a hold of how we express our artistic...", "How Art Could Save The World", 31, "how-art-could-save-the-world", 125 },
                    { new Guid("82c7b648-56c1-4a64-bc40-4760ec7b37a5"), null, new Guid("131c273d-bbab-47d3-ac4e-f8cd7f3c25a0"), new DateTime(2022, 8, 17, 9, 56, 50, 147, DateTimeKind.Local).AddTicks(7265), "The superior read instructions from a thin manual: early the next morning, a Sunday, they should go to any 7-Eleven and use their white card at the store’s A.T.M. They could not use a regular bank A.T.M., or one in another convenience store. The gangsters should each withdraw a hundred thousand yen at a time (about nine hundred dollars) but make no more than nineteen transactions per machine. If anybody made twenty withdrawals from a single A.T.M., his card would be blocked. Withdrawals could start at 5 a.m. and continue until 8 a.m. The volunteers were told to choose the Japanese language when prompted—an indication, Shimomura realized, that the cards were foreign. After making nineteen withdrawals, they should wait an hour before visiting another 7-Eleven. They could keep ten per cent of the cash. The rest would go to the bosses. Finally, each volunteer was told to memorize a pin.", new DateTime(2022, 8, 17, 9, 56, 50, 147, DateTimeKind.Local).AddTicks(7264), true, 11, "The country’s cyber forces have raked in billions of dollars for the regime by pulling off schemes ranging from A.T.M. heists to cryptocurrency...", "The Incredible Rise of North Korea’s Hacking Army", 41, "the-incredible-rise-of-north-koreas-hacking-army", 147 },
                    { new Guid("9ae5d581-035b-4c41-875b-ad06ede73a17"), null, new Guid("1175989d-97f4-4e70-b1fa-2d8e88d905a7"), new DateTime(2022, 8, 17, 9, 56, 50, 147, DateTimeKind.Local).AddTicks(7275), "It's been a while since my last journal. I was so caught up with exam and my Helpx trip in Finland, it had been the only thing in my mind for days. I had never been so far up North until that trip to Uusi-Värtsilä, if I had a Russian visa at the time, I could go to Russia in a couple of hours. Uusi-Värtsilä is an abandoned village, it has less than 200 inhabitants, no public transport and the nearest supermarket is 7 km away. Yes, I was literally in the middle of nowhere. I enjoyed so much being in the nature in Finland. Water is never far away. Dense forests always await somewhere nearby. The first time I was in an open bog, I thought I was teleported in another planet. The working tasks in Dennis' place were interesting and pretty relaxing as well. We harvested the wild garlics, went picking wild berries and mushrooms in the forest, made the traditional Finnish bread, took care of the green house garden and the house. One minus point though the sleeping arrangements there were chaotic, we had to share our rooms and my roommate was a self-centered character. I rarely found my privacy in the house so it drained my energy sometimes. When my roommate left, we all felt like we just woke up from a nightmare.", new DateTime(2022, 8, 17, 9, 56, 50, 147, DateTimeKind.Local).AddTicks(7275), false, 5, "Life It's been a while since my last journal. I was so caught up with exam and my Helpx trip in Finland, it had been the only thing...", "The Times Of Our Lives.", 20, "the-times-of-our-lives", 100 },
                    { new Guid("fb11b0e4-c7e0-4e1f-9ae8-0f8aadd24c1e"), null, new Guid("0a43ec60-ba72-4929-a669-d30d23f00700"), new DateTime(2022, 8, 17, 9, 56, 50, 147, DateTimeKind.Local).AddTicks(7279), "Ever since Neil Armstrong imprinted his footsteps on the Lunar surface, bringing new laurels to international space exploration endeavors and effectively ending the Space Race, humanity has dreamed even more of one day settling down in space and conquering the stars. Enormous amounts of financial budget coupled with thousands of hours of research efforts throughout the years by global space programs and organizations (most prominently NASA) are gradually and laboriously bringing the human race closer to fulfillment of this centuries-long ambition. Yet, insurmountable physical challenges exist, and with them comes the uncertainty of long-term human settlement on other planets. What lies waiting for us ahead in the vastness of space? What are the requirements and how do we settle down on those planets? In fact, does the term space colonization really mean that humans are really able to conquer the entirety of the 93-billion-light-year observable universe?", new DateTime(2022, 8, 17, 9, 56, 50, 147, DateTimeKind.Local).AddTicks(7278), true, 11, "https://www.facebook.com/notes/nguy%E1%BB%85n-t%C3%A0i-long/space-colonization-what-are-hindering-us/1112384025483978 Ever since...", "Space colonization: What are hindering us?", 23, "space-colonization-what-are-hindering-us", 111 }
                });

            migrationBuilder.InsertData(
                table: "Comment",
                columns: new[] { "Id", "CommentHeader", "CommentText", "CommentTime", "Email", "Name", "PostId" },
                values: new object[,]
                {
                    { new Guid("16c74205-589d-47ad-a06b-fdd584b1e029"), "Lorem Ipsum is simply", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it", new DateTimeOffset(new DateTime(2022, 8, 17, 9, 56, 50, 147, DateTimeKind.Unspecified).AddTicks(7346), new TimeSpan(0, 7, 0, 0, 0)), "liang@gmail.com", "Liang", new Guid("7c9e6679-7425-40de-944b-e07fc1f90ae7") },
                    { new Guid("20d92c8b-02a9-4581-9647-ad01cf5256c9"), "Lorem ipsum dolor sit amet", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it", new DateTimeOffset(new DateTime(2022, 8, 17, 9, 56, 50, 147, DateTimeKind.Unspecified).AddTicks(7368), new TimeSpan(0, 7, 0, 0, 0)), "samoyajohns@gmail.com", "Samoya Johns", new Guid("0f8fad5b-d9cb-469f-a165-70867728950e") },
                    { new Guid("d3ade39a-f957-4119-ac58-74efeba3e017"), "Lorem ipsum dolor sit amet", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.", new DateTimeOffset(new DateTime(2022, 8, 17, 9, 56, 50, 147, DateTimeKind.Unspecified).AddTicks(7365), new TimeSpan(0, 7, 0, 0, 0)), "coreyoates@gmail.com", "Corey oates", new Guid("fb11b0e4-c7e0-4e1f-9ae8-0f8aadd24c1e") }
                });

            migrationBuilder.InsertData(
                table: "PostTagMap",
                columns: new[] { "PostId", "TagId" },
                values: new object[,]
                {
                    { new Guid("0f8fad5b-d9cb-469f-a165-70867728950e"), new Guid("e5353cd0-50eb-4762-915e-0c91f026c108") },
                    { new Guid("465ee23d-c44a-41e4-98a0-5a41754b6607"), new Guid("17483cc0-5c58-4144-963d-4023840e801e") },
                    { new Guid("465ee23d-c44a-41e4-98a0-5a41754b6607"), new Guid("f89e3fa9-d563-472a-ae4d-d29ffc53c913") },
                    { new Guid("7c9e6679-7425-40de-944b-e07fc1f90ae7"), new Guid("17483cc0-5c58-4144-963d-4023840e801e") },
                    { new Guid("7c9e6679-7425-40de-944b-e07fc1f90ae7"), new Guid("8f116ef5-8ec8-4d97-9a48-4a2fe660a547") },
                    { new Guid("7c9e6679-7425-40de-944b-e07fc1f90ae7"), new Guid("a7be5849-51fa-494f-a38c-78fdc7ea8151") },
                    { new Guid("82c7b648-56c1-4a64-bc40-4760ec7b37a5"), new Guid("8f116ef5-8ec8-4d97-9a48-4a2fe660a547") },
                    { new Guid("fb11b0e4-c7e0-4e1f-9ae8-0f8aadd24c1e"), new Guid("17483cc0-5c58-4144-963d-4023840e801e") },
                    { new Guid("fb11b0e4-c7e0-4e1f-9ae8-0f8aadd24c1e"), new Guid("a7be5849-51fa-494f-a38c-78fdc7ea8151") }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Category_AppUserId",
                table: "Category",
                column: "AppUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Comment_PostId",
                table: "Comment",
                column: "PostId");

            migrationBuilder.CreateIndex(
                name: "IX_Post_AppUserId",
                table: "Post",
                column: "AppUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Post_CategoryId",
                table: "Post",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_PostTagMap_TagId",
                table: "PostTagMap",
                column: "TagId");

            migrationBuilder.CreateIndex(
                name: "IX_Tag_AppUserId",
                table: "Tag",
                column: "AppUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Comment");

            migrationBuilder.DropTable(
                name: "PostTagMap");

            migrationBuilder.DropTable(
                name: "RoleClaims");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "UserClaims");

            migrationBuilder.DropTable(
                name: "UserLogin");

            migrationBuilder.DropTable(
                name: "UserRole");

            migrationBuilder.DropTable(
                name: "UserToken");

            migrationBuilder.DropTable(
                name: "Post");

            migrationBuilder.DropTable(
                name: "Tag");

            migrationBuilder.DropTable(
                name: "Category");

            migrationBuilder.DropTable(
                name: "AppUser");
        }
    }
}
