﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FA.JustBlog.Entity.Migrations
{
    public partial class updatepass : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AppUser",
                keyColumn: "Id",
                keyValue: new Guid("c952f359-6b89-4723-9567-ec477aef3009"));

            migrationBuilder.InsertData(
                table: "AppUser",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "Name", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { new Guid("2a0d49f5-bbe7-4396-82ef-ac6f4dbc37f7"), 0, "31f995cf-fb09-4ad6-ad62-bb72a477142f", "hihi@gmail.com", false, false, null, "hihi@gmail.com", null, null, "AQAAAAEAACcQAAAAEDwimoikhti285Svgl9wDyKvqHkXU+7UZk/97Wv2GqjWVoAnv+A8kBtqgO6W/wTMJw==", null, false, "b11a3877-5396-45bb-aa5e-e24208235714", false, "hihi@gmail.com" });

            migrationBuilder.UpdateData(
                table: "Comment",
                keyColumn: "Id",
                keyValue: new Guid("16c74205-589d-47ad-a06b-fdd584b1e029"),
                column: "CommentTime",
                value: new DateTimeOffset(new DateTime(2022, 8, 29, 21, 42, 40, 185, DateTimeKind.Unspecified).AddTicks(2632), new TimeSpan(0, 7, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "Comment",
                keyColumn: "Id",
                keyValue: new Guid("20d92c8b-02a9-4581-9647-ad01cf5256c9"),
                column: "CommentTime",
                value: new DateTimeOffset(new DateTime(2022, 8, 29, 21, 42, 40, 185, DateTimeKind.Unspecified).AddTicks(2653), new TimeSpan(0, 7, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "Comment",
                keyColumn: "Id",
                keyValue: new Guid("d3ade39a-f957-4119-ac58-74efeba3e017"),
                column: "CommentTime",
                value: new DateTimeOffset(new DateTime(2022, 8, 29, 21, 42, 40, 185, DateTimeKind.Unspecified).AddTicks(2649), new TimeSpan(0, 7, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "Post",
                keyColumn: "Id",
                keyValue: new Guid("0f8fad5b-d9cb-469f-a165-70867728950e"),
                columns: new[] { "Modified", "PostedOn" },
                values: new object[] { new DateTime(2022, 8, 29, 21, 42, 40, 185, DateTimeKind.Local).AddTicks(2523), new DateTime(2022, 8, 29, 21, 42, 40, 185, DateTimeKind.Local).AddTicks(2522) });

            migrationBuilder.UpdateData(
                table: "Post",
                keyColumn: "Id",
                keyValue: new Guid("465ee23d-c44a-41e4-98a0-5a41754b6607"),
                column: "Modified",
                value: new DateTime(2022, 8, 29, 21, 42, 40, 185, DateTimeKind.Local).AddTicks(2499));

            migrationBuilder.UpdateData(
                table: "Post",
                keyColumn: "Id",
                keyValue: new Guid("7c9e6679-7425-40de-944b-e07fc1f90ae7"),
                columns: new[] { "Modified", "PostedOn" },
                values: new object[] { new DateTime(2022, 8, 29, 21, 42, 40, 185, DateTimeKind.Local).AddTicks(2526), new DateTime(2022, 8, 29, 21, 42, 40, 185, DateTimeKind.Local).AddTicks(2526) });

            migrationBuilder.UpdateData(
                table: "Post",
                keyColumn: "Id",
                keyValue: new Guid("82c7b648-56c1-4a64-bc40-4760ec7b37a5"),
                columns: new[] { "Modified", "PostedOn" },
                values: new object[] { new DateTime(2022, 8, 29, 21, 42, 40, 185, DateTimeKind.Local).AddTicks(2519), new DateTime(2022, 8, 29, 21, 42, 40, 185, DateTimeKind.Local).AddTicks(2518) });

            migrationBuilder.UpdateData(
                table: "Post",
                keyColumn: "Id",
                keyValue: new Guid("9ae5d581-035b-4c41-875b-ad06ede73a17"),
                columns: new[] { "Modified", "PostedOn" },
                values: new object[] { new DateTime(2022, 8, 29, 21, 42, 40, 185, DateTimeKind.Local).AddTicks(2530), new DateTime(2022, 8, 29, 21, 42, 40, 185, DateTimeKind.Local).AddTicks(2530) });

            migrationBuilder.UpdateData(
                table: "Post",
                keyColumn: "Id",
                keyValue: new Guid("fb11b0e4-c7e0-4e1f-9ae8-0f8aadd24c1e"),
                columns: new[] { "Modified", "PostedOn" },
                values: new object[] { new DateTime(2022, 8, 29, 21, 42, 40, 185, DateTimeKind.Local).AddTicks(2534), new DateTime(2022, 8, 29, 21, 42, 40, 185, DateTimeKind.Local).AddTicks(2533) });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { new Guid("7b9ce8c2-a281-4019-9fcf-ff0a8f86e040"), "09cc210e-0647-46da-85a5-462f484f63bf", "hihi", "hihi", "hihi" });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { new Guid("7b9ce8c2-a281-4019-9fcf-ff0a8f86e040"), new Guid("2a0d49f5-bbe7-4396-82ef-ac6f4dbc37f7") });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AppUser",
                keyColumn: "Id",
                keyValue: new Guid("2a0d49f5-bbe7-4396-82ef-ac6f4dbc37f7"));

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: new Guid("7b9ce8c2-a281-4019-9fcf-ff0a8f86e040"));

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { new Guid("7b9ce8c2-a281-4019-9fcf-ff0a8f86e040"), new Guid("2a0d49f5-bbe7-4396-82ef-ac6f4dbc37f7") });

            migrationBuilder.InsertData(
                table: "AppUser",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "Name", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { new Guid("c952f359-6b89-4723-9567-ec477aef3009"), 0, "2882ee4c-ba29-4c23-b6e4-908e4d1d37af", null, false, false, null, "Admin", null, null, null, null, false, null, false, null });

            migrationBuilder.UpdateData(
                table: "Comment",
                keyColumn: "Id",
                keyValue: new Guid("16c74205-589d-47ad-a06b-fdd584b1e029"),
                column: "CommentTime",
                value: new DateTimeOffset(new DateTime(2022, 8, 17, 9, 56, 50, 147, DateTimeKind.Unspecified).AddTicks(7346), new TimeSpan(0, 7, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "Comment",
                keyColumn: "Id",
                keyValue: new Guid("20d92c8b-02a9-4581-9647-ad01cf5256c9"),
                column: "CommentTime",
                value: new DateTimeOffset(new DateTime(2022, 8, 17, 9, 56, 50, 147, DateTimeKind.Unspecified).AddTicks(7368), new TimeSpan(0, 7, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "Comment",
                keyColumn: "Id",
                keyValue: new Guid("d3ade39a-f957-4119-ac58-74efeba3e017"),
                column: "CommentTime",
                value: new DateTimeOffset(new DateTime(2022, 8, 17, 9, 56, 50, 147, DateTimeKind.Unspecified).AddTicks(7365), new TimeSpan(0, 7, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "Post",
                keyColumn: "Id",
                keyValue: new Guid("0f8fad5b-d9cb-469f-a165-70867728950e"),
                columns: new[] { "Modified", "PostedOn" },
                values: new object[] { new DateTime(2022, 8, 17, 9, 56, 50, 147, DateTimeKind.Local).AddTicks(7268), new DateTime(2022, 8, 17, 9, 56, 50, 147, DateTimeKind.Local).AddTicks(7268) });

            migrationBuilder.UpdateData(
                table: "Post",
                keyColumn: "Id",
                keyValue: new Guid("465ee23d-c44a-41e4-98a0-5a41754b6607"),
                column: "Modified",
                value: new DateTime(2022, 8, 17, 9, 56, 50, 147, DateTimeKind.Local).AddTicks(7249));

            migrationBuilder.UpdateData(
                table: "Post",
                keyColumn: "Id",
                keyValue: new Guid("7c9e6679-7425-40de-944b-e07fc1f90ae7"),
                columns: new[] { "Modified", "PostedOn" },
                values: new object[] { new DateTime(2022, 8, 17, 9, 56, 50, 147, DateTimeKind.Local).AddTicks(7271), new DateTime(2022, 8, 17, 9, 56, 50, 147, DateTimeKind.Local).AddTicks(7271) });

            migrationBuilder.UpdateData(
                table: "Post",
                keyColumn: "Id",
                keyValue: new Guid("82c7b648-56c1-4a64-bc40-4760ec7b37a5"),
                columns: new[] { "Modified", "PostedOn" },
                values: new object[] { new DateTime(2022, 8, 17, 9, 56, 50, 147, DateTimeKind.Local).AddTicks(7265), new DateTime(2022, 8, 17, 9, 56, 50, 147, DateTimeKind.Local).AddTicks(7264) });

            migrationBuilder.UpdateData(
                table: "Post",
                keyColumn: "Id",
                keyValue: new Guid("9ae5d581-035b-4c41-875b-ad06ede73a17"),
                columns: new[] { "Modified", "PostedOn" },
                values: new object[] { new DateTime(2022, 8, 17, 9, 56, 50, 147, DateTimeKind.Local).AddTicks(7275), new DateTime(2022, 8, 17, 9, 56, 50, 147, DateTimeKind.Local).AddTicks(7275) });

            migrationBuilder.UpdateData(
                table: "Post",
                keyColumn: "Id",
                keyValue: new Guid("fb11b0e4-c7e0-4e1f-9ae8-0f8aadd24c1e"),
                columns: new[] { "Modified", "PostedOn" },
                values: new object[] { new DateTime(2022, 8, 17, 9, 56, 50, 147, DateTimeKind.Local).AddTicks(7279), new DateTime(2022, 8, 17, 9, 56, 50, 147, DateTimeKind.Local).AddTicks(7278) });
        }
    }
}
