﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Entity.Entity
{
    public class Post
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string PostContent { get; set; }
        public string UrlSlug { get; set; }
        public bool Published { get; set; }
        public DateTime PostedOn { get; set; }
        public DateTime Modified { get; set; }
        public virtual Guid CategoryId { get; set; }
        public virtual Category Category { get; set; }
        public int ViewCount { get; set; }

        public int RateCount { get; set; }

        public int TotalRate { get; set; }

        [NotMapped]
        public decimal Rate { get => RateCount = TotalRate / RateCount; }

        public virtual IList<PostTagMap> PostTagMaps { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }

   
    }
}
