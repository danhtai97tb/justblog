﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Entity.Entity
{
    public class AppUser : IdentityUser<Guid>
    {
        public string Name { get; set; }
        public ICollection<Post> Posts { get; set; } 
        public ICollection<Tag> Tags { get; set; }
        public ICollection<Category> Category { get; set; }
    }
}
