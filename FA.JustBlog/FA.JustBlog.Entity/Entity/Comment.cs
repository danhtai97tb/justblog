﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Entity.Entity
{
    public class Comment
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string CommentHeader { get; set; }

        public string CommentText { get; set; }

        public DateTimeOffset CommentTime { get; set; }

        public virtual Guid PostId { get; set; }

        public virtual Post Post { get; set; }
    }
}
