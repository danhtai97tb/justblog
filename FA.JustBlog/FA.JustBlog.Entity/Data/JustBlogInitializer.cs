﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FA.JustBlog.Entity.Entity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace FA.JustBlog.Entity.Data
{
    public static class JustBlogInitializer
    {

        public static void Seed(this ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Category>().HasData(

                  new Category()
                  {
                      Id = new Guid("131c273d-bbab-47d3-ac4e-f8cd7f3c25a0"),
                      Name = "Travel",
                      UrlSlug = "travel",
                      Description = "Travel Blog"
                  },
                new Category()
                {
                    Id = new Guid("fd6cd3bb-58a3-4541-b419-647d1e1d820c"),
                    Name = "Recipe",
                    UrlSlug = "recipe",
                    Description = "Recipe Blog"
                },
                new Category()
                {
                    Id = new Guid("1175989d-97f4-4e70-b1fa-2d8e88d905a7"),
                    Name = "Tips",
                    UrlSlug = "tips",
                    Description = "Tips Blog"
                },
                new Category()
                {
                    Id = new Guid("0a43ec60-ba72-4929-a669-d30d23f00700"),
                    Name = "Life Style",
                    UrlSlug = "life-style",
                    Description = "Life Style Blog"
                }

         );

            modelBuilder.Entity<Post>().HasData(
                new Post()
                {
                    Id = new Guid("465ee23d-c44a-41e4-98a0-5a41754b6607"),
                    Title = "What to do when you have to wait",
                    UrlSlug = "what-to-do-when-you-have-to-wait",
                    ShortDescription = "Wait for the next delayed flight in 3 hours. Wait on the train for 12 hours before arrival. Wait for someone who is never going to...",
                    PostContent = "Wait for the next delayed flight in 3 hours. Wait on the train for 12 hours before arrival. Wait for someone who is never going to text you back. Wait for something that is never going to fall in your mouth. Wait for a story where you could become the main character. Wait for the inevitable conclusion that awaits everyone.",
                    Published = true,
                    PostedOn = DateTime.Parse("10/08/2020"),
                    Modified = DateTime.Now,
                    RateCount = 10,
                    TotalRate = 46,
                    ViewCount = 112,
                    CategoryId = Guid.Parse("131c273d-bbab-47d3-ac4e-f8cd7f3c25a0")

                },
                new Post()
                {

                    Id = new Guid("82c7b648-56c1-4a64-bc40-4760ec7b37a5"),
                    Title = "The Incredible Rise of North Korea’s Hacking Army",
                    UrlSlug = "the-incredible-rise-of-north-koreas-hacking-army",
                    ShortDescription = "The country’s cyber forces have raked in billions of dollars for the regime by pulling off schemes ranging from A.T.M. heists to cryptocurrency...",
                    PostContent = "The superior read instructions from a thin manual: early the next morning, a Sunday, they should go to any 7-Eleven and use their white card at the store’s A.T.M. They could not use a regular bank A.T.M., or one in another convenience store. The gangsters should each withdraw a hundred thousand yen at a time (about nine hundred dollars) but make no more than nineteen transactions per machine. If anybody made twenty withdrawals from a single A.T.M., his card would be blocked. Withdrawals could start at 5 a.m. and continue until 8 a.m. The volunteers were told to choose the Japanese language when prompted—an indication, Shimomura realized, that the cards were foreign. After making nineteen withdrawals, they should wait an hour before visiting another 7-Eleven. They could keep ten per cent of the cash. The rest would go to the bosses. Finally, each volunteer was told to memorize a pin.",
                    Published = true,
                    PostedOn = DateTime.Now,
                    Modified = DateTime.Now,
                    RateCount = 11,
                    TotalRate = 41,
                    ViewCount = 147,
                    CategoryId = Guid.Parse("131c273d-bbab-47d3-ac4e-f8cd7f3c25a0")
                },
                new Post()
                {
                    Id = new Guid("0f8fad5b-d9cb-469f-a165-70867728950e"),
                    Title = "A Tay on Wheels",
                    UrlSlug = "a-tay-on-wheels",
                    ShortDescription = "Motorbikes in Hanoi. Alright, so it’s been a while since my last (and first and only) post. I could come up with any number of excuses...",
                    PostContent = "Yes, the traffic is annoying and crazy. I often count my lucky stars for not knowing a lot of dirty language in Vietnamese, because I’d be spouting it a lot. At people that absolutely need to pass me on the left 2 meters before they take a right turn. At people who feel that while driving a motorbike is the perfect time for them to check their Facebook comments. At people who seem to think their horn creates a force field, so they keep it blaring while they speed through tiny openings in traffic with no regard for anyone else or even themselves.",
                    Published = true,
                    PostedOn = DateTime.Now,
                    Modified = DateTime.Now,
                    RateCount = 13,
                    TotalRate = 31,
                    ViewCount = 125,
                    CategoryId = Guid.Parse("fd6cd3bb-58a3-4541-b419-647d1e1d820c")
                },
                new Post()
                {
                    Id = new Guid("7c9e6679-7425-40de-944b-e07fc1f90ae7"),
                    Title = "How Art Could Save The World",
                    UrlSlug = "how-art-could-save-the-world",
                    ShortDescription = "It’s extremely difficult to define “art” in modern times. As of recent, postmodernism has taken a hold of how we express our artistic...",
                    PostContent = "We are naturally drawn to statements that could resonate with us and our place in society. The existential and political angst the artist faces in modern times as they’re conveying ideas, through conventional means of art. The recent Joker movie is a prime example of how a fictional character could be representative and even reflective of certain group(s) of people, in this case, the mentally ill. The character Arthur Fleck (presumably based on respectively the two classic Martin’s characters Travis Bickle and Rupert Pupkin) should give us a clear, albeit twisted image of a modern tragedy: He’s a mentally ill loner who’s also got a medical condition where he can’t control his laugh. He’s misunderstood, isolated and mistreated by society, who harbors dangerous perspectives about life and about himself.",
                    Published = true,
                    PostedOn = DateTime.Now,
                    Modified = DateTime.Now,
                    RateCount = 13,
                    TotalRate = 31,
                    ViewCount = 125,
                    CategoryId = Guid.Parse("fd6cd3bb-58a3-4541-b419-647d1e1d820c")
                },
                new Post()
                {
                    Id = new Guid("9ae5d581-035b-4c41-875b-ad06ede73a17"),
                    Title = "The Times Of Our Lives.",
                    UrlSlug = "the-times-of-our-lives",
                    ShortDescription = "Life It's been a while since my last journal. I was so caught up with exam and my Helpx trip in Finland, it had been the only thing...",
                    PostContent = "It's been a while since my last journal. I was so caught up with exam and my Helpx trip in Finland, it had been the only thing in my mind for days. I had never been so far up North until that trip to Uusi-Värtsilä, if I had a Russian visa at the time, I could go to Russia in a couple of hours. Uusi-Värtsilä is an abandoned village, it has less than 200 inhabitants, no public transport and the nearest supermarket is 7 km away. Yes, I was literally in the middle of nowhere. I enjoyed so much being in the nature in Finland. Water is never far away. Dense forests always await somewhere nearby. The first time I was in an open bog, I thought I was teleported in another planet. The working tasks in Dennis' place were interesting and pretty relaxing as well. We harvested the wild garlics, went picking wild berries and mushrooms in the forest, made the traditional Finnish bread, took care of the green house garden and the house. One minus point though the sleeping arrangements there were chaotic, we had to share our rooms and my roommate was a self-centered character. I rarely found my privacy in the house so it drained my energy sometimes. When my roommate left, we all felt like we just woke up from a nightmare.",
                    PostedOn = DateTime.Now,
                    Modified = DateTime.Now,
                    RateCount = 5,
                    TotalRate = 20,
                    ViewCount = 100,
                    CategoryId = Guid.Parse("1175989d-97f4-4e70-b1fa-2d8e88d905a7")
                },
                new Post()
                {
                    Id = new Guid("fb11b0e4-c7e0-4e1f-9ae8-0f8aadd24c1e"),
                    Title = "Space colonization: What are hindering us?",
                    UrlSlug = "space-colonization-what-are-hindering-us",
                    ShortDescription = "https://www.facebook.com/notes/nguy%E1%BB%85n-t%C3%A0i-long/space-colonization-what-are-hindering-us/1112384025483978 Ever since...",
                    PostContent = "Ever since Neil Armstrong imprinted his footsteps on the Lunar surface, bringing new laurels to international space exploration endeavors and effectively ending the Space Race, humanity has dreamed even more of one day settling down in space and conquering the stars. Enormous amounts of financial budget coupled with thousands of hours of research efforts throughout the years by global space programs and organizations (most prominently NASA) are gradually and laboriously bringing the human race closer to fulfillment of this centuries-long ambition. Yet, insurmountable physical challenges exist, and with them comes the uncertainty of long-term human settlement on other planets. What lies waiting for us ahead in the vastness of space? What are the requirements and how do we settle down on those planets? In fact, does the term space colonization really mean that humans are really able to conquer the entirety of the 93-billion-light-year observable universe?",
                    Published = true,
                    PostedOn = DateTime.Now,
                    Modified = DateTime.Now,
                    RateCount = 11,
                    TotalRate = 23,
                    ViewCount = 111,
                    CategoryId = Guid.Parse("0a43ec60-ba72-4929-a669-d30d23f00700")

                }
                );
            modelBuilder.Entity<Tag>().HasData(
                new Tag()
                {
                    Id = new Guid("f6597c20-249d-48c0-aba7-4cd21cef8b69"),
                    Name = "travel",
                    UrlSlug = "travel",
                    Description = "Travel Tag",
                    Count = 1
                },
                new Tag()
                {
                    Id = new Guid("8f116ef5-8ec8-4d97-9a48-4a2fe660a547"),
                    Name = "food",
                    UrlSlug = "food",
                    Description = "food Tag",
                    Count = 2
                },
                new Tag()
                {
                    Id = new Guid("e5353cd0-50eb-4762-915e-0c91f026c108"),
                    Name = "recipe",
                    UrlSlug = "recipe",
                    Description = "recipe Tag",
                    Count = 3
                },
                new Tag()
                {
                    Id = new Guid("a7be5849-51fa-494f-a38c-78fdc7ea8151"),
                    Name = "tips",
                    UrlSlug = "tips",
                    Description = "tips Tag"
                },
                new Tag()
                {
                    Id = new Guid("17483cc0-5c58-4144-963d-4023840e801e"),
                    Name = "study",
                    UrlSlug = "study",
                    Description = "study Tag"
                },
                new Tag()
                {
                    Id = new Guid("f89e3fa9-d563-472a-ae4d-d29ffc53c913"),
                    Name = "life style",
                    UrlSlug = "life-style",
                    Description = "life style Tag"
                }
                );
            modelBuilder.Entity<PostTagMap>().HasData(
                new PostTagMap()
                {
                    PostId = Guid.Parse("465ee23d-c44a-41e4-98a0-5a41754b6607"),
                    TagId = Guid.Parse("f89e3fa9-d563-472a-ae4d-d29ffc53c913"),
                },
                new PostTagMap()
                {
                    PostId = Guid.Parse("465ee23d-c44a-41e4-98a0-5a41754b6607"),
                    TagId = Guid.Parse("17483cc0-5c58-4144-963d-4023840e801e"),
                },
                new PostTagMap()
                {
                    PostId = Guid.Parse("82c7b648-56c1-4a64-bc40-4760ec7b37a5"),
                    TagId = Guid.Parse("8f116ef5-8ec8-4d97-9a48-4a2fe660a547")
                },
                new PostTagMap()
                {
                    PostId = Guid.Parse("0f8fad5b-d9cb-469f-a165-70867728950e"),
                    TagId = Guid.Parse("e5353cd0-50eb-4762-915e-0c91f026c108")
                },
                new PostTagMap()
                {
                    PostId = Guid.Parse("7c9e6679-7425-40de-944b-e07fc1f90ae7"),
                    TagId = Guid.Parse("17483cc0-5c58-4144-963d-4023840e801e")
                },
                new PostTagMap()
                {
                    PostId = Guid.Parse("7c9e6679-7425-40de-944b-e07fc1f90ae7"),
                    TagId = Guid.Parse("a7be5849-51fa-494f-a38c-78fdc7ea8151")
                },
                new PostTagMap()
                {
                    PostId = Guid.Parse("fb11b0e4-c7e0-4e1f-9ae8-0f8aadd24c1e"),
                    TagId = Guid.Parse("a7be5849-51fa-494f-a38c-78fdc7ea8151")
                },
                new PostTagMap()
                {
                    PostId = Guid.Parse("fb11b0e4-c7e0-4e1f-9ae8-0f8aadd24c1e"),
                    TagId = Guid.Parse("17483cc0-5c58-4144-963d-4023840e801e")
                },
                new PostTagMap()
                {
                    PostId = Guid.Parse("7c9e6679-7425-40de-944b-e07fc1f90ae7"),
                    TagId = Guid.Parse("8f116ef5-8ec8-4d97-9a48-4a2fe660a547")
                }
                );
            modelBuilder.Entity<Comment>().HasData(
                new Comment()
                {
                    Id = new Guid("16c74205-589d-47ad-a06b-fdd584b1e029"),
                    Name = "Liang",
                    Email = "liang@gmail.com",
                    CommentHeader = "Lorem Ipsum is simply",
                    CommentText = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it",
                    CommentTime = DateTime.Now,
                    PostId = Guid.Parse("7c9e6679-7425-40de-944b-e07fc1f90ae7")
                },
                new Comment()
                {
                    Id = new Guid("d3ade39a-f957-4119-ac58-74efeba3e017"),
                    Name = "Corey oates",
                    Email = "coreyoates@gmail.com",
                    CommentHeader = "Lorem ipsum dolor sit amet",
                    CommentText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
                    CommentTime = DateTime.Now,
                    PostId = Guid.Parse("fb11b0e4-c7e0-4e1f-9ae8-0f8aadd24c1e")
                },
                new Comment()
                {
                    Id = new Guid("20d92c8b-02a9-4581-9647-ad01cf5256c9"),
                    Name = "Samoya Johns",
                    Email = "samoyajohns@gmail.com",
                    CommentHeader = "Lorem ipsum dolor sit amet",
                    CommentText = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it",
                    CommentTime = DateTime.Now,
                    PostId = Guid.Parse("0f8fad5b-d9cb-469f-a165-70867728950e")
                }
                );
            modelBuilder.Entity<AppRole>().HasData(
               new AppRole()
               {
                   Id = new Guid("7b9ce8c2-a281-4019-9fcf-ff0a8f86e040"),
                   Name = "hihi",
                   Description = "hihi",
                   NormalizedName = "hihi"
               }
               );

            var passwordHasher = new PasswordHasher<AppUser>();
            var user = new AppUser()
            {
                Id = new Guid("2a0d49f5-bbe7-4396-82ef-ac6f4dbc37f7"),
                UserName = "hihi@gmail.com",
                Name = "hihi@gmail.com",
                Email = "hihi@gmail.com",
                SecurityStamp = Guid.NewGuid().ToString("D"),
                LockoutEnabled = false,
            };
            user.PasswordHash = passwordHasher.HashPassword(user, "Admin@123");
            modelBuilder.Entity<AppUser>().HasData(
              user
                );
            modelBuilder.Entity<IdentityUserRole<Guid>>().HasData(
                new IdentityUserRole<Guid>()
                {
                    RoleId = Guid.Parse("7b9ce8c2-a281-4019-9fcf-ff0a8f86e040"),
                    UserId = Guid.Parse("2a0d49f5-bbe7-4396-82ef-ac6f4dbc37f7")
                }
                );
        }
    }
}
