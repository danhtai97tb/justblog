﻿using FA.JustBlog.Repository.Infrastructures;
using FA.JustBlog.Service.CategoryServices;
using Microsoft.AspNetCore.Mvc;

namespace FA.JustBlog.MVC.VIewComponents
{
    public class MenuCategory : ViewComponent
    {
        private readonly ICategoryService categoryService;
        public MenuCategory(ICategoryService categoryService)
        {
            this.categoryService = categoryService;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var category = categoryService.GetAll();
            return await Task.FromResult(View("MenuCategory", category.DataList));
        }
    }
}
