﻿using FA.JustBlog.Repository.Infrastructures;
using Microsoft.AspNetCore.Mvc;

namespace FA.JustBlog.MVC.VIewComponents
{
    public class PorpularTag : ViewComponent
    {
        private readonly IUnitOfWork unitOfWork;
        public PorpularTag(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var tag = unitOfWork.TagRepository.GetAll().Take(10);
            return await Task.FromResult(View("_PorpularTag", tag));
        }
    }
}
