﻿using FA.JustBlog.Repository.Infrastructures;
using FA.JustBlog.Service.PostService;
using Microsoft.AspNetCore.Mvc;

namespace FA.JustBlog.MVC.VIewComponents
{
    public class LastestPost : ViewComponent
    {
        private readonly IPostService postService;
        public LastestPost(IPostService postService)
        {
            this.postService = postService;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var post = postService.GetLastestPost(5);
            return await Task.FromResult(View(post));  
        }
    }
}
