﻿using FA.JustBlog.Repository.Infrastructures;
using FA.JustBlog.Service.PostService;
using Microsoft.AspNetCore.Mvc;

namespace FA.JustBlog.MVC.VIewComponents
{
    public class MostViewPost : ViewComponent
    {
        private readonly IPostService postService;
        public MostViewPost(IPostService postService)
        {
            this.postService = postService;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var post = postService.GetMostViewPost(5);
            return await Task.FromResult(View("_MostViewPost" ,post));
        }
    }
}
