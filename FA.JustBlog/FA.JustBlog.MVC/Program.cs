using FA.JustBlog.Entity.Data;
using FA.JustBlog.Entity.Entity;
using FA.JustBlog.Models.Models;
using FA.JustBlog.Repository.Infrastructures;
using FA.JustBlog.Repository.IRepository;
using FA.JustBlog.Repository.Repository;
using FA.JustBlog.Service.CategoryServices;
using FA.JustBlog.Service.CommentService;
using FA.JustBlog.Service.Mapper;
using FA.JustBlog.Service.PostService;
using FA.JustBlog.Service.TagService;
using FA.JustBlog.Service.UserService;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic;
using NToastNotify;
using System;
using NLog;
using NLog.Web;

var logger = NLog.LogManager.Setup().LoadConfigurationFromAppSettings().GetCurrentClassLogger();
logger.Debug("init main");

try
{
    var builder = WebApplication.CreateBuilder(args);

    // Add services to the container.
    builder.Services.AddControllersWithViews(
        options =>
        {
            options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute());
        }
    );
    builder.Services.AddControllersWithViews().AddNToastNotifyToastr(new ToastrOptions()
    {
        ProgressBar = false,
        PositionClass = ToastPositions.TopRight,
        TimeOut = 1000
    });
    builder.Services.AddDbContext<JustBlogContext>(options =>
            options.UseSqlServer(builder.Configuration.GetConnectionString("DatabaseConection")));

    builder.Services.AddScoped<FA.JustBlog.Repository.Infrastructures.IUnitOfWork, UnitOfWork>();
    builder.Services.AddScoped<ICategoryService, CategoryService>();
    builder.Services.AddScoped<IPostService, PostService>();
    builder.Services.AddScoped<ITagService, TagService>();
    builder.Services.AddScoped<ICommentService, CommentService>();
    builder.Services.AddAutoMapper(typeof(MapperConfig));
    builder.Services.AddIdentity<AppUser, AppRole>(options =>
    {
        options.Password.RequireUppercase = false;
        options.Password.RequiredLength = 6;
        options.Password.RequireDigit = false;
        options.Password.RequireNonAlphanumeric = false;
        options.Password.RequireLowercase = false;
        options.SignIn.RequireConfirmedEmail = false;
        options.SignIn.RequireConfirmedPhoneNumber = false;
    }).AddEntityFrameworkStores<JustBlogContext>();
    builder.Services.AddScoped<IAppUserService, AppUserService>();


    var app = builder.Build();

    // Configure the HTTP request pipeline.
    if (!app.Environment.IsDevelopment())
    {
        app.UseExceptionHandler("/Home/Error");
        // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
        app.UseHsts();
    }

    app.UseHttpsRedirection();
    app.UseStaticFiles();
    app.UseNToastNotify();
    app.UseRouting();

    app.UseAuthentication();
    app.UseAuthorization();
    app.MapControllerRoute(
        name: "post",
        pattern: "post/{year}/{month}/{title}/{id?}",
        defaults: new { controller = "Post", action = "Details" },
         new { year = @"\d{4}", month = @"\d{2}" }
        );

    app.MapControllerRoute(
        name: "admin",
        pattern: "{controller = Admin}/{action = index}/{id?}"
        );

    app.MapControllerRoute(
        name: "default",
        pattern: "{controller=Post}/{action=Index}/{id?}");


    app.Run();
}
catch (Exception exception)
{
    // NLog: catch setup errors
    logger.Error(exception, "Stopped program because of exception");
    throw;
}
finally
{
    // Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
    NLog.LogManager.Shutdown();
}

