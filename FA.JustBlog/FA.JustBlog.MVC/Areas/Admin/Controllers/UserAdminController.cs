﻿using FA.JustBlog.Entity.Entity;
using FA.JustBlog.Models.AppUser;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Net;

namespace FA.JustBlog.MVC.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("Admin")]
    public class UserAdminController : Controller
    {
        private readonly UserManager<AppUser> userManager;
        private readonly RoleManager<AppRole> roleManager;


        public UserAdminController(UserManager<AppUser> userManager, RoleManager<AppRole> roleManager)
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
        }
        [Route("IndexUser")]
        [Authorize(Roles = "Contributor, Blog Owner")]
        public async Task<IActionResult> Index()
        {
            var y = await userManager.Users.ToListAsync();
            return View(y);
        }
        [Route("DetailUser")]
        [HttpGet]
        [Authorize(Roles = "Contributor, Blog Owner")]
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            var user = await userManager.FindByIdAsync(id);

            ViewBag.RoleNames = await userManager.GetRolesAsync(user);

            return View(user);
        }
        [Route("CreateUser")]
        [HttpGet]
        [Authorize(Roles = "Blog Owner")]
        public async Task<IActionResult> Create()
        {
            //Get the list of Roles
            ViewBag.RoleId = new SelectList(await roleManager.Roles.ToListAsync(), "Name", "Name");
            return View();
        }
        [Route("CreateUser")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Blog Owner")]
        public async Task<IActionResult> Create(RegisterViewModel userViewModel, params string[] selectedRoles)
        {
            if (ModelState.IsValid)
            {
                var user = new AppUser()
                {
                    UserName = userViewModel.Email,
                    Email = userViewModel.Email,
                    Name = userViewModel.Email
                };
                var result = await userManager.CreateAsync(user, userViewModel.Password);

                //Add User to the selected Roles 
                if (result.Succeeded)
                {
                    if (selectedRoles != null)
                    {
                        var results = await userManager.AddToRolesAsync(user, selectedRoles);
                        if (!results.Succeeded)
                        {
                            ModelState.AddModelError("", "Failed");
                            ViewBag.RoleId = new SelectList(await roleManager.Roles.ToListAsync(), "Name", "Name");
                            return View();
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Error");
                    ViewBag.RoleId = new SelectList(roleManager.Roles, "Name", "Name");
                    return View();

                }
                return RedirectToAction("Index");
            }
            ViewBag.RoleId = new SelectList(roleManager.Roles, "Name", "Name");
            return View();
        }

        [Route("EditUser")]
        [HttpGet]
        [Authorize(Roles = "Blog Owner")]
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            var user = await userManager.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            var userRoles = await userManager.GetRolesAsync(user);
            var roles = await roleManager.Roles.ToListAsync();
            var roleItems = roles.Select(role =>
                 new SelectListItem(
                     role.Name,
                     role.Id.ToString(),
                     userRoles.Any(ur => ur.Contains(role.Name)))).ToList();
            var result = new UserViewModel()
            {
                Email = user.Email,
                Id = user.Id.ToString(),
                RolesList = roleItems
            };

            return View(result);
        }
        [Route("EditUser")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Blog Owner")]
        public async Task<IActionResult> Edit(UserViewModel editUser)
        {
            if (ModelState.IsValid)
            {
                var user = await userManager.FindByIdAsync(editUser.Id);
                if (user == null)
                {
                    return NotFound();
                }
                var userRoles = await userManager.GetRolesAsync(user);

                var rolesToAdd = new List<string>();
                var rolesToDelete = new List<string>();
                foreach (var role in editUser.RolesList)
                {
                    var assignedInDb = userRoles.FirstOrDefault(ur => ur == role.Text);
                    if (role.Selected)
                    {
                        if (assignedInDb == null)
                        {
                            rolesToAdd.Add(role.Text);
                        }
                    }
                    else
                    {
                        if (assignedInDb != null)
                        {
                            rolesToDelete.Add(role.Text);
                        }
                    }
                }
                if (rolesToAdd.Any())
                {
                    await userManager.AddToRolesAsync(user, rolesToAdd);
                }
                if (rolesToDelete.Any())
                {
                    await userManager.RemoveFromRolesAsync(user, rolesToDelete);
                }

                user.Name = editUser.Email;
                user.Email = editUser.Email;
                userManager.UpdateAsync(user);

                return RedirectToAction("Index");
            }
            ModelState.AddModelError("", "Something failed.");
            return View(editUser);
        }
        [Route("DeleteUser")]
        [Authorize(Roles = "Blog Owner")]

        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            var user = await userManager.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            var result = await userManager.DeleteAsync(user);
            if (!result.Succeeded)
            {
                ModelState.AddModelError("", "Delete Failed");
                return View();
            }
            return RedirectToAction("Index");
            return View();
        }

       
    }
}
