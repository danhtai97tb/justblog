﻿using FA.JustBlog.Models.Models;
using FA.JustBlog.Service.TagService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Razor.Language;
using NToastNotify;
using System.Data;
using X.PagedList;

namespace FA.JustBlog.MVC.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("admin")]
    [Authorize(Roles = "Contributor, Blog Owner")]
    public class TagManager : Controller
    {
        private readonly ITagService tagService;
        private readonly IToastNotification _toastNotification;

        public TagManager(ITagService tagService , IToastNotification toastNotification)
        {
            this.tagService = tagService;
            this._toastNotification = toastNotification;
        }
        [Route("ListTag")]
        public IActionResult ListTag(int? page)
        {
            var tag = tagService.GetAll();
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(tag.DataList.ToPagedList(pageNumber,pageSize));
        }
        [Route("CreateTag")]
        public IActionResult Create()
        {
            return View();
        }
        [Route("CreateTag")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(TagModel tag)
        {
            if (ModelState.IsValid)
            {
                var result = tagService.Create(tag);
                if (result.IsSuccessed)
                {
                    _toastNotification.AddSuccessToastMessage(result.Message);
                }
                else
                {
                    _toastNotification.AddErrorToastMessage(result.Message);
                }

                return RedirectToAction("ListTag");
            }
            return View(tag);
        }
        [Route("EditTag")]
        public IActionResult Edit(Guid id)
        {
            var tag = tagService.Find(id);
            return View(tag.Data);
        }
        [Route("EditTag")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(TagModel model)
        {
            if (ModelState.IsValid)
            {
                var result = tagService.Update(model);
                if (result.IsSuccessed)
                {
                    _toastNotification.AddSuccessToastMessage(result.Message);
                }
                else
                {
                    _toastNotification.AddErrorToastMessage(result.Message);
                }
                return RedirectToAction("ListTag");
            }
            return View(model);
           
        }

        [Route("DeleteTag")]
        [Authorize(Roles = "Blog Owner")]
        public IActionResult Delete(Guid id)
        {
            var result = tagService.Delete(id);
            if (result.IsSuccessed)
            {
                _toastNotification.AddSuccessToastMessage(result.Message);
            }
            else
            {
                _toastNotification.AddErrorToastMessage(result.Message);
            }
            return RedirectToAction("ListTag");
        }
    }
}
