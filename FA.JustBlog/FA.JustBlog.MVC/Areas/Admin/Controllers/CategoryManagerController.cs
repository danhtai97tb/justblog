﻿using FA.JustBlog.Entity.Entity;
using FA.JustBlog.Models.Models;
using FA.JustBlog.Repository.Infrastructures;
using FA.JustBlog.Service.CategoryServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NToastNotify;
using System.Data;
using System.Net;
using X.PagedList;

namespace FA.JustBlog.MVC.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("admin")]
    [Authorize(Roles = "Contributor, Blog Owner")]
    public class CategoryManagerController : Controller
    {
        private readonly ICategoryService _categoryServices;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IToastNotification _toastNotification;
        private readonly ILogger<CategoryManagerController> _logger;

        public CategoryManagerController(ICategoryService categoryServices, IUnitOfWork unitOfWork, IToastNotification toastNotification, ILogger<CategoryManagerController> logger)
        {
            _unitOfWork = unitOfWork;
            _categoryServices = categoryServices;
            _toastNotification = toastNotification;
            _logger = logger;
        }

        [Route("list")]
        public IActionResult List(int? page)
        {
            try
            {
                var category = _categoryServices.GetAll();
                int pageSize = 5;
                int pageNumber = (page ?? 1);
                return View(category.DataList.ToPagedList(pageNumber, pageSize));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
            }
            return View();

        }

        [Route("DetailCategory")]
        public IActionResult Detail(Guid id)
        {
            try
            {
                var categories = _categoryServices.GetByCategoryId(id);
                return View(categories.Data);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
            }
            return View();
        }

        [Route("create")]
        public IActionResult Create()
        {
            return View();
        }
        [Route("create")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(CategoryModel categoryModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = _categoryServices.Create(categoryModel);
                    if (result.IsSuccessed)
                    {
                        _toastNotification.AddSuccessToastMessage(result.Message);
                    }
                    else
                    {
                        _toastNotification.AddErrorToastMessage(result.Message);
                    }
                    if (result != null)
                    {
                        TempData["Message"] = "Insert successful!";
                    }
                    else
                    {
                        TempData["Message"] = "Insert failed!";
                    }
                    return RedirectToAction("List");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
            }

            return View(categoryModel);
        }

        [Route("edit")]
        public IActionResult Edit(Guid Id)
        {
            var categories = _categoryServices.GetByCategoryId(Id);
            return View(categories.Data);
        }
        [Route("edit")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(CategoryModel categoryModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = _categoryServices.Update(categoryModel);
                    if (result.IsSuccessed)
                    {
                        _toastNotification.AddSuccessToastMessage(result.Message);
                    }
                    else
                    {
                        _toastNotification.AddErrorToastMessage(result.Message);
                    }
                    if (result != null)
                    {
                        TempData["Message"] = "Update successful!";
                    }
                    else
                    {
                        TempData["Message"] = "Update failed!";
                    }
                    return RedirectToAction("List");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
            }

            return View(categoryModel);
        }

        [Route("deleteCategory")]
        [Authorize(Roles = "Blog Owner")]
        public IActionResult Delete(Guid id)
        {
            try
            {
                var result = _categoryServices.Delete(id);
                if (result.IsSuccessed)
                {
                    _toastNotification.AddSuccessToastMessage(result.Message);
                }
                else
                {
                    _toastNotification.AddErrorToastMessage(result.Message);
                }
                return RedirectToAction("List");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
            }
            return View();
        }
    }
}
