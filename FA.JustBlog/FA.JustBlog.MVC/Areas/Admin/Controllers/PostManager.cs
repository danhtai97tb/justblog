﻿using AutoMapper;
using FA.JustBlog.Entity.Entity;
using FA.JustBlog.Models.Models;
using FA.JustBlog.Models.Models.Post;
using FA.JustBlog.Repository.Infrastructures;
using FA.JustBlog.Service.CategoryServices;
using FA.JustBlog.Service.PostService;
using FA.JustBlog.Service.TagService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.TagHelpers.Cache;
using NToastNotify;
using System.Data;
using System.Net.WebSockets;
using X.PagedList;

namespace FA.JustBlog.MVC.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("admin")]
    [Authorize(Roles = "Contributor, Blog Owner")]
    public class PostManager : Controller
    {
        private readonly IPostService _postService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ITagService _tagService;
        private readonly ICategoryService _categoryService;
        private readonly IMapper mapper;
        private readonly IToastNotification _toastNotification;
        private readonly ILogger<PostManager> _logger;
        public PostManager(IPostService postService, IUnitOfWork unitOfWork, ITagService tagService, ICategoryService categoryService, IMapper mapper, IToastNotification toastNotification, ILogger<PostManager> logger)
        {
            _unitOfWork = unitOfWork;
            _postService = postService;
            _tagService = tagService;
            _categoryService = categoryService;
            this.mapper = mapper;
            this._toastNotification = toastNotification;
            _logger = logger;
        }

        [Route("ShowPost")]
        public IActionResult ShowPost()
        {
            return View();
        }

        [Route("LastestPost")]
        public IActionResult LastestPost(int? page)
        {
            try
            {
                var list = _postService.GetLastestPost(10);
                int pageSize = 5;
                int pageNumber = (page ?? 1);
                return View(list.ToPagedList(pageNumber, pageSize));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
            }
            return View();

        }

        [Route("MostViewPost")]
        public IActionResult MostViewPost(int? page)
        {
            try
            {
                var list = _postService.GetMostViewPost(10);
                int pageSize = 5;
                int pageNumber = (page ?? 1);
                return View(list.ToPagedList(pageNumber, pageSize));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
            }
            return View();

        }

        [Route("PublishedPost")]
        public IActionResult PublishedPost(int? page)
        {
            try
            {
                var list = _postService.GetPublishedPost();
                int pageSize = 5;
                int pageNumber = (page ?? 1);
                return View(list.ToPagedList(pageNumber, pageSize));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
            }
            return View();

        }
        [Route("ListPost")]
        public IActionResult ListPost(int? page)
        {
            try
            {
                var list = _postService.GetAll();
                int pageSize = 5;
                int pageNumber = (page ?? 1);
                return View(list.ToPagedList(pageNumber, pageSize));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
            }

            return View();

        }

        [Route("CreatePost")]
        public IActionResult Create()
        {
            ViewBag.Category = new SelectList(_unitOfWork.CategoryRepository.GetAll(), "Id", "Name");
            return View();
        }
        [Route("CreatePost")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(CreatePostViewModel postModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = _postService.Create(postModel);
                    ViewBag.PublishedList = new List<SelectListItem>()
                {
                      new SelectListItem() { Text = "True", Value = true.ToString(), Selected=true },
                      new SelectListItem() { Text = "False", Value = false.ToString(), Selected=false },
                 };
                    if (result.IsSuccessed)
                    {
                        _toastNotification.AddSuccessToastMessage(result.Message);
                    }
                    else
                    {
                        _toastNotification.AddErrorToastMessage(result.Message);
                    }

                    return RedirectToAction("ListPost");

                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
            }

            return View(postModel);

        }

        [Route("EditPost")]
        public IActionResult Edit(Guid id)
        {
            ViewBag.Category = new SelectList(_unitOfWork.CategoryRepository.GetAll(), "Id", "Name");
            var post = _postService.GetPostById(id);
            ViewBag.Tag = new SelectList(_unitOfWork.PostRepository.GetTag(id), "Id", "Name");
            return View(post.Data);
        }
        [Route("EditPost")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(EditPostViewModel model)
        {
            try
            {
                var result = _postService.Update(model);
                if (result.IsSuccessed)
                {
                    _toastNotification.AddSuccessToastMessage(result.Message);
                }
                else
                {
                    _toastNotification.AddErrorToastMessage(result.Message);
                }

                return RedirectToAction("ListPost");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
            }


            return View(model);
        }
        [Route("DeletePost")]
        [Authorize(Roles = "Blog Owner")]
        public IActionResult Delete(Guid id)
        {
            try
            {
                var result = _postService.Delete(id);
                if (result.IsSuccessed)
                {
                    _toastNotification.AddSuccessToastMessage(result.Message);
                }
                else
                {
                    _toastNotification.AddErrorToastMessage(result.Message);
                }
                return RedirectToAction("ListPost");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
            }

            return View();

        }
    }
}
