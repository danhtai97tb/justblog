﻿using FA.JustBlog.Models.Models;
using FA.JustBlog.Repository.Infrastructures;
using FA.JustBlog.Service.CategoryServices;
using FA.JustBlog.Service.CommentService;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using NToastNotify;
using X.PagedList;

namespace FA.JustBlog.MVC.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("admin")]
    public class CommentManager : Controller
    {
        private readonly ICommentService _commentService;
        private readonly IUnitOfWork unitOfWork;
        private readonly IToastNotification _toastNotification;
        private readonly ILogger<CommentManager> _logger;
        public CommentManager(ICommentService commentService, IUnitOfWork unitOfWork, IToastNotification toastNotification, ILogger<CommentManager> logger)
        {
            _commentService = commentService;
            this.unitOfWork = unitOfWork;
            _toastNotification = toastNotification;
            _logger = logger;
        }

        [Route("ListCommnet")]
        public IActionResult ListComment(int? page)
        {
            try
            {
                var comment = _commentService.GetAll();
                int pageSize = 5;
                int pageNumber = (page ?? 1);
                return View(comment.DataList.ToPagedList(pageNumber, pageSize));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
            }
            return View();

        }
        [Route("CreateComment")]
        public IActionResult Create()
        {
            ViewBag.Post = new SelectList(unitOfWork.PostRepository.GetAll(), "Id", "Title");
            return View();
        }
        [Route("CreateComment")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(CommentModel model)
        {
            try
            {
                var result = _commentService.Create(model);
                if (result.IsSuccessed)
                {
                    _toastNotification.AddSuccessToastMessage(result.Message);
                }
                else
                {
                    _toastNotification.AddErrorToastMessage(result.Message);
                }
                return RedirectToAction("ListComment");

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
            }


            return View(model);
        }
        [Route("EditComment")]
        public IActionResult Edit(Guid id)
        {
            ViewBag.Post = new SelectList(unitOfWork.PostRepository.GetAll(), "Id", "Title");
            var comment = _commentService.Find(id);
            return View(comment.Data);
        }
        [Route("EditComment")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(CommentModel model)
        {
            try
            {

                var result = _commentService.Update(model);
                if (result.IsSuccessed)
                {
                    _toastNotification.AddSuccessToastMessage(result.Message);
                }
                else
                {
                    _toastNotification.AddErrorToastMessage(result.Message);
                }
                return RedirectToAction("ListComment");

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
            }

            return View(model);
        }
        [Route("DeleteCommnet")]
        public IActionResult Delete(Guid id)
        {
            try
            {

                var result = _commentService.Delete(id);
                if (result.IsSuccessed)
                {
                    _toastNotification.AddSuccessToastMessage(result.Message);
                }
                else
                {
                    _toastNotification.AddErrorToastMessage(result.Message);
                }
                return RedirectToAction("ListComment");

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
            }
            return View();
        }
    }
}
