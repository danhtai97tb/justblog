﻿using FA.JustBlog.Entity.Entity;
using FA.JustBlog.Models.AppRole;
using FA.JustBlog.Models.AppUser;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Data;
using System.Drawing;
using System.Net;

namespace FA.JustBlog.MVC.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("Admin")]
    
    public class RolesAdminController : Controller
    {
        private RoleManager<AppRole> _roleManager;

        public RolesAdminController(RoleManager<AppRole> roleManager)
        {
            _roleManager = roleManager;
        }
        [Route("indexIdentity")]
        [Authorize(Roles = "Contributor, Blog Owner")]
        public IActionResult Index()
        {
            return View(_roleManager.Roles);
        }
        [Route("CreateRole")]
        [HttpGet]
        [Authorize(Roles = "Blog Owner")]
        public IActionResult Create()
        {
            return View();
        }
        [Route("CreateRole")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Blog Owner")]

        public async Task<IActionResult> Create(CreateRoleViewModel createRoleViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(createRoleViewModel);
            }
            var appRole = new AppRole()
            {
                Name = createRoleViewModel.Name,
                Description = createRoleViewModel.Description,

            };
            var resutl = await _roleManager.CreateAsync(appRole);
            if (resutl.Succeeded)
            {
                return RedirectToAction("Index");
            }
            ModelState.AddModelError(string.Empty, "Not Found");
            return View(createRoleViewModel);
        }

        [Route("EditRole")]
        [Authorize(Roles = "Blog Owner")]
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            var role = await _roleManager.FindByIdAsync(id);
            if (role == null)
            {
                return NotFound();
            }
            var edit = new EditRoleViewModel();
            edit.Id = role.Id;
            edit.Name = role.Name;

            return View(edit);
        }
        [Route("EditRole")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Blog Owner")]
        public async Task<IActionResult> Edit([Bind("Name,Id")] EditRoleViewModel roleModel)
        {
            if (ModelState.IsValid)
            {
                var role = await _roleManager.FindByIdAsync(roleModel.Id.ToString());
                role.Name = roleModel.Name;
                await _roleManager.UpdateAsync(role);
                return RedirectToAction("Index");
            }


            return View(roleModel);
        }

        [Route("DeleteRole")]
        [Authorize(Roles = "Blog Owner")]
        public async Task<IActionResult> Delete(string id)
        {
            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    return BadRequest();
                }
                var role = await _roleManager.FindByIdAsync(id);
                if (role == null)
                {
                    return NotFound();
                }
                IdentityResult result;
                result = await _roleManager.DeleteAsync(role);

                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", "Delete Faild");
                    return View();
                }
                return RedirectToAction("Index");
            }
            return View();
        }

    }
}
