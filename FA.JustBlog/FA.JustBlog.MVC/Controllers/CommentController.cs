﻿using FA.JustBlog.Entity.Entity;
using FA.JustBlog.Models.Models;
using FA.JustBlog.Repository.Infrastructures;
using FA.JustBlog.Service.CommentService;
using Microsoft.AspNetCore.Mvc;
using NToastNotify;

namespace FA.JustBlog.MVC.Controllers
{

    public class CommentController : Controller
    {
        private readonly ICommentService commentService;
        private readonly IUnitOfWork unitOfWork;
        private readonly IToastNotification _toastNotification;

        public CommentController(ICommentService commentService, IUnitOfWork unitOfWork, IToastNotification toastNotification )
        {
            this.commentService = commentService;
            this.unitOfWork = unitOfWork;
            _toastNotification = toastNotification;
        }

        public IActionResult Index(Guid id)
        {
            var comment = commentService.GetCommentsForPost(id);
            return View(comment.DataList);
        }
        [HttpGet("comment/add")]
        public IActionResult AddComment(Guid postId, string name, string email, string commentHeader ,string commentText)
        {
            var comment = new Comment();
            comment.PostId = postId;
            comment.Name = name;
            comment.CommentHeader = commentHeader;
            comment.Email = email;
            comment.CommentText = commentText;
            comment.CommentTime = DateTime.Now;
            unitOfWork.CommentRepository.Add(comment);
            unitOfWork.SaveChanges();

            return Json(comment);

            //var res = commentService.Create(postId,name,email,commentHeader,commentText);
            //if (!res.IsSuccessed)
            //{
            //    _toastNotification.AddErrorToastMessage(res.ErrorMessage);
            //}
            //return Ok();
        }
    }
}
