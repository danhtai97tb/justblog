﻿using System.Diagnostics;
using FA.JustBlog.MVC.Models;
using FA.JustBlog.Repository.Infrastructures;
using FA.JustBlog.Repository.IRepository;
using Microsoft.AspNetCore.Mvc;

namespace FA.JustBlog.MVC.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IUnitOfWork _unitOfWork;
        public HomeController(ILogger<HomeController> logger , IUnitOfWork unitOfWork)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
        }
        [HttpGet]
        public IActionResult Index()
        {
            var post = _unitOfWork.PostRepository.GetAll();
            return View(post);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        
        
        public IActionResult AboutCard()
        {
            return PartialView("_PartialAboutCard");
        }
    }
}