﻿using FA.JustBlog.Repository.Infrastructures;
using FA.JustBlog.Service.CategoryServices;
using Microsoft.AspNetCore.Mvc;

namespace FA.JustBlog.MVC.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ICategoryService _categoryService;
        public CategoryController(ILogger<HomeController> logger, ICategoryService categoryService)
        {
            _logger = logger;
            _categoryService = categoryService;
        }
        public IActionResult Index()
        {
            return View();
        }

    }
}
