﻿using FA.JustBlog.Entity.Entity;
using FA.JustBlog.Models.AppUser;
using FA.JustBlog.Service.UserService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NToastNotify;
using System.Security.Claims;

namespace FA.JustBlog.MVC.Controllers
{
    
    public class AccountController : Controller
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly RoleManager<AppRole> _roleManager;
        private readonly IToastNotification _toastNotification;
        private readonly ILogger<AccountController> logger;


        public AccountController(UserManager<AppUser> userManager, SignInManager<AppUser> signInManager, RoleManager<AppRole> roleManager, IToastNotification toastNotification, ILogger<AccountController> logger)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _toastNotification = toastNotification;
            this.logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]

        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel loginViewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(loginViewModel);
                }
                var user = await _signInManager.UserManager.FindByNameAsync(loginViewModel.Email);
                if (user == null)
                {
                    ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                    return View();
                }
                var respon = await _signInManager.CheckPasswordSignInAsync(user, loginViewModel.Password, false);

                if (respon.Succeeded)
                {
                    var claim = new List<Claim>
                {
                    new Claim("name" , "pass")
                };
                    var roles = await _signInManager.UserManager.GetRolesAsync(user);
                    if (roles.Any())
                    {
                        var roleClaim = string.Join(",", roles);
                        claim.Add(new Claim("Roles", roleClaim));

                    }
                    await _signInManager.SignInWithClaimsAsync(user, loginViewModel.RememberMe, claim);
                    logger.LogInformation("User logged in.");
                    return RedirectToAction("Index", "Post");
                }
                ModelState.AddModelError(string.Empty, "Not Found");
            }
            catch(Exception ex)
            {
                logger.LogError(ex, "");
            }
           
            return View(loginViewModel);
        }

        [HttpGet]
      
        public IActionResult Register()
        {
            return View();
        }
        [HttpPost]

        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel registerViewModel)
        {
            try
            {
                var appUser = new AppUser()
                {
                    Email = registerViewModel.Email,
                    UserName = registerViewModel.Email,
                    Name = registerViewModel.Email,
                };
                var respon = await _userManager.CreateAsync(appUser, registerViewModel.Password);
                if (respon.Succeeded)
                {
                    logger.LogInformation("User Register.");
                    return RedirectToAction("Login");

                }
            }
            catch(Exception ex)
            {
                logger.LogError(ex, "");
            }
            return View(registerViewModel);
        }
        [Route("logout")]
        [AllowAnonymous]
        public async Task<IActionResult> Logout()
        {
            try
            {
                await _signInManager.SignOutAsync();
                logger.LogInformation("User Log Out");
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "");
            }
            return RedirectToAction("Index", "Post");
        }

        [AllowAnonymous]
        public string AccessDenied()
        {
            return "Da dang nhap, nhung khong co quyen";
            
        }
    }
}
