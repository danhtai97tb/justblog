﻿
using FA.JustBlog.Models.Models.Post;
using FA.JustBlog.Service.PostService;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using X.PagedList;

namespace FA.JustBlog.MVC.Controllers
{
    public class PostController : Controller
    {
        private readonly IPostService _postService;
        private readonly ILogger<PostController> _logger;
        public PostController(IPostService postService, ILogger<PostController> logger)
        {
            _postService = postService;
            _logger = logger;
        }

        public IActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            try
            {
                var post = _postService.GetAll();
                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;
                }

                ViewBag.CurrentFilter = searchString;

                if (!String.IsNullOrEmpty(searchString))
                {
                    post = post.Where(x => x.Title.Contains(searchString.Trim()));
                }
                int pageSize = 10;
                int pageNumber = (page ?? 1);
                return View(post.ToPagedList(pageNumber, pageSize));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");

            }
            return View();
        }




        public IActionResult GetPostByCategory(string category)
        {
            try
            {
                var post = _postService.GetPostByCategory(category);
                if (post == null)
                {
                    return BadRequest();
                }
                return View(post);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
            }
            return View();
        }

        public IActionResult GetPostByTag(string tag)
        {
            try
            {
                var post = _postService.GetPostByTag(tag);
                if (post == null)
                {
                    return BadRequest();
                }
                return View(post);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
            }
            return View();


        }
        public IActionResult Detail(Guid id)
        {
            try
            {
                var post = _postService.GetByPostId(id);
                if (post == null)
                {
                    return BadRequest();
                }
                return View(post);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
            }
            return View();

        }

        public IActionResult Details(int year, int month, string title)
        {
            try
            {
                var posts = _postService.GetPostByMonthYear(year, month, title);
                if (posts == null)
                {
                    return BadRequest();
                }
                return View("Detail", posts);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
            }
            return View();
        }



    }
}
