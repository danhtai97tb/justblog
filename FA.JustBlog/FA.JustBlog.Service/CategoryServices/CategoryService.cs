﻿using FA.Common;
using FA.JustBlog.Entity.Entity;
using FA.JustBlog.Models.Models;
using FA.JustBlog.Models.Response;
using FA.JustBlog.Repository.Infrastructures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Service.CategoryServices
{
    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWork _unitOfWork;
        public CategoryService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public ResponseResult<CategoryModel> Create(CategoryModel request)
        {
            try
            {
                var category = new Category()
                {
                    Name = request.Name,
                    UrlSlug = SeoUrlHepler.FrientlyUrl(request.Name),
                    Description = request.Description,
                };
                _unitOfWork.CategoryRepository.Add(category);
                _unitOfWork.SaveChanges();
                return new ResponseResult<CategoryModel>() { Data = request, StatusCode = 201 };
            }
            catch (Exception ex)
            {
                return new ResponseResult<CategoryModel>(ex.Message + ex.InnerException.Message);
            }
        }

        public ResponseResult<CategoryModel> Delete(CategoryModel request)
        {
            try
            {
                var category = new Category()
                {
                    Name = request.Name,
                    Id = request.Id,
                    Description = request.Description,
                    UrlSlug = request.UrlSlug
                };
                _unitOfWork.CategoryRepository.Delete(category);
                _unitOfWork.SaveChanges();
                return new ResponseResult<CategoryModel>() { Data = request, StatusCode = 201 };
            }
            catch (Exception ex)
            {
                return new ResponseResult<CategoryModel>(ex.Message + ex.InnerException.Message);
            }
        }

        public ResponseResult<CategoryModel> Delete(Guid id)
        {
            try
            {
                var checkModel = _unitOfWork.CategoryRepository.Find(id);
                if (checkModel != null)
                {
                    _unitOfWork.CategoryRepository.Delete(id);
                    _unitOfWork.SaveChanges();
                    return new ResponseResult<CategoryModel>();
                }
                return new ResponseResult<CategoryModel>($"Not found the Category has id = {id}");
            }
            catch (Exception ex)
            {
                return new ResponseResult<CategoryModel>(ex.Message + ex.InnerException.Message);
            }

        }

        public ResponseResult<CategoryModel> GetAll()
        {
            try
            {
                var cate = _unitOfWork.CategoryRepository.GetAll().ToList();
                var list = new List<CategoryModel>();
                if (cate != null)
                {
                    foreach(var item in cate)
                    {
                        var model = new CategoryModel();
                        model.Id = item.Id;
                        model.Name = item.Name;
                        model.UrlSlug = item.UrlSlug;
                        model.Description = item.Description;
                        list.Add(model);
                    }
                    return new ResponseResult<CategoryModel>() { DataList = list };
                }
                return new ResponseResult<CategoryModel>("No have any data!");
            } 
            catch (Exception ex)
            {
                return new ResponseResult<CategoryModel>(ex.Message + ex.InnerException.Message);
            }
           
             

        }

        public ResponseResult<CategoryModel> GetByCategoryId(Guid categoryId)
        {
            try
            {
                var categoryExist = _unitOfWork.CategoryRepository.Find(categoryId);
                if (categoryExist == null)
                {
                    return new ResponseResult<CategoryModel>("Category not existing in the database") { StatusCode = 404 };
                }
                var category = new CategoryModel()
                {
                    Id = categoryExist.Id,
                    Name = categoryExist.Name,
                    Description = categoryExist.Description,
                    UrlSlug = categoryExist.UrlSlug

                };
                return new ResponseResult<CategoryModel>() { Data = category };
            }
            catch (Exception ex)
            {
                return new ResponseResult<CategoryModel>(ex.Message + ex.InnerException.Message);
            }
            
            
           
        }

        public ResponseResult<CategoryModel> Update(CategoryModel request)
        {
            try
            {
                var categoryExist = _unitOfWork.CategoryRepository.Find(request.Id);
                if(categoryExist == null)
                {
                    return new ResponseResult<CategoryModel>("Category not existing in the database") { StatusCode = 404 };
                }
                
                categoryExist.Name = request.Name;
                categoryExist.Description = request.Description;
                categoryExist.UrlSlug = request.UrlSlug;
                _unitOfWork.CategoryRepository.Update(categoryExist);
                _unitOfWork.SaveChanges();
                return new ResponseResult<CategoryModel>() { Data = request };
            }
            catch (Exception ex)
            {
                return new ResponseResult<CategoryModel>(ex.Message + ex.InnerException.Message);
            }


        }

        
    }
    
}
