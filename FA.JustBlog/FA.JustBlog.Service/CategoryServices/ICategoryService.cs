﻿using FA.JustBlog.Entity.Entity;
using FA.JustBlog.Models.Models;
using FA.JustBlog.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Service.CategoryServices
{
    public interface ICategoryService
    {
        public ResponseResult<CategoryModel> Create(CategoryModel request);
        public ResponseResult<CategoryModel> Delete(CategoryModel request);
        public ResponseResult<CategoryModel> GetAll();
        public ResponseResult<CategoryModel> GetByCategoryId(Guid categoryId);

        public ResponseResult<CategoryModel> Update(CategoryModel request);

        public ResponseResult<CategoryModel> Delete(Guid id);

    }
}
