﻿using FA.JustBlog.Entity.Entity;
using FA.JustBlog.Models.AppUser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Service.UserService
{
    public interface IAppUserService
    {
        public Task<AppUser> Login (LoginViewModel loginViewModel);
        public AppUser Register (RegisterViewModel registerViewModel);


    }
}
