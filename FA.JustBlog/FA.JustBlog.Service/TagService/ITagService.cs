﻿using FA.JustBlog.Models.Models;
using FA.JustBlog.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Service.TagService
{
    public interface ITagService
    {
        ResponseResult<TagModel> Find(Guid tagId);
        ResponseResult<TagModel> GetTagByUrlSlug(string urlSlug);
        ResponseResult<TagModel> GetAll();
        ResponseResult<TagModel> Create(TagModel model);
        ResponseResult<TagModel> Update(TagModel model);
        ResponseResult<TagModel> Delete(TagModel model);
        ResponseResult<TagModel> Delete(Guid Id);

       

    }
}
