﻿using AutoMapper;
using FA.Common;
using FA.JustBlog.Entity.Entity;
using FA.JustBlog.Models.Models;
using FA.JustBlog.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Service.TagService
{
    public class TagService : ITagService
    {
        private readonly Repository.Infrastructures.IUnitOfWork _unitOfWork;
        private readonly IMapper mapper;
        public TagService(Repository.Infrastructures.IUnitOfWork unitOfWork, IMapper mapper)
        {
            this._unitOfWork = unitOfWork;
            this.mapper = mapper;
        }
        public ResponseResult<TagModel> Create(TagModel model)
        {
            try
            {
                var tag = new Tag()
                {
                    Name = model.Name,
                    UrlSlug = SeoUrlHepler.FrientlyUrl(model.Name),
                    Description = model.Description,
                };
                _unitOfWork.TagRepository.Add(tag);
                _unitOfWork.SaveChanges();
                return new ResponseResult<TagModel>() { Data = model, StatusCode = 201 };
            }
            catch (Exception ex)
            {
                return new ResponseResult<TagModel>(ex.Message + ex.InnerException.Message);
            }
        }

        public ResponseResult<TagModel> Delete(TagModel model)
        {
            throw new NotImplementedException();
        }

        public ResponseResult<TagModel> Delete(Guid Id)
        {
            try
            {
                var checkModel = _unitOfWork.TagRepository.Find(Id);
                if (checkModel != null)
                {
                    _unitOfWork.TagRepository.Delete(Id);
                    _unitOfWork.SaveChanges();
                    return new ResponseResult<TagModel>();
                }
                return new ResponseResult<TagModel>($"Not found the Tag has Id = {Id}");

            }
            catch (Exception ex)
            {
                return new ResponseResult<TagModel>(ex.Message + ex.InnerException.Message);
            }

        }

        public ResponseResult<TagModel> Find(Guid tagId)
        {
            try
            {
                var result = _unitOfWork.TagRepository.Find(tagId);
                if (result == null)
                {
                    return new ResponseResult<TagModel>($"Can't find the category that has id = {tagId}");
                }
                var model = mapper.Map<TagModel>(result);
                return new ResponseResult<TagModel>() { Data = model };
            }
            catch (Exception ex)
            {
                return new ResponseResult<TagModel>(ex.Message + ex.InnerException.Message);
            }

        }

        public ResponseResult<TagModel> GetAll()
        {
            var result = _unitOfWork.TagRepository.GetAll().ToList();
            if (result.Count() > 0)
            {
                var models = mapper.Map<IEnumerable<TagModel>>(result).ToList();
                //var models = result.Select(x => new TagModel(x)).ToList();
                return new ResponseResult<TagModel>() { DataList = models };
            }
            return new ResponseResult<TagModel>("No have any data!");


        }

        public ResponseResult<TagModel> GetTagByUrlSlug(string urlSlug)
        {
            throw new NotImplementedException();
        }

        public ResponseResult<TagModel> Update(TagModel model)
        {
            try
            {
                if (model != null)
                {
                    var entity = mapper.Map<Tag>(model);
                    _unitOfWork.TagRepository.Update(entity);
                    _unitOfWork.SaveChanges();
                    return new ResponseResult<TagModel>() { Data = model };
                }
                return new ResponseResult<TagModel>("No have data to Update Tag!");
            }
            catch (Exception ex)
            {
                return new ResponseResult<TagModel>(ex.Message + ex.InnerException.Message);
            }

        }
    }
}
