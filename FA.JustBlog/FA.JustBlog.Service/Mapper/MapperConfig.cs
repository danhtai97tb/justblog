﻿using AutoMapper;
using FA.JustBlog.Entity.Entity;
using FA.JustBlog.Models.Models;
using FA.JustBlog.Models.Models.Post;

namespace FA.JustBlog.Service.Mapper
{
    public class MapperConfig : Profile
    {
        public MapperConfig()
        {
            CreateMap<Category,CategoryModel>().ReverseMap();
            CreateMap<Post,PostModel>().ReverseMap();
            CreateMap<Tag,TagModel>().ReverseMap();
            CreateMap<Comment,CommentModel>().ReverseMap(); 
        }
    }
}
