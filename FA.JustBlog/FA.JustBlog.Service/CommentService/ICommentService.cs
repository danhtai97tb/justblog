﻿using FA.JustBlog.Models.Models;
using FA.JustBlog.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Service.CommentService
{
    public interface ICommentService
    {
        ResponseResult<CommentModel> Find(Guid commentId);
        ResponseResult<CommentModel> GetCommentsForPost(Guid postId);
        ResponseResult<CommentModel> GetAll();
        ResponseResult<CommentModel> Create(CommentModel model);
        ResponseResult<CommentModel> Create(Guid postId, string commentName, string commentEmail, string commentTitle, string commentBody);
        ResponseResult<CommentModel> Update(CommentModel model);
        ResponseResult<CommentModel> Delete(CommentModel model);
        ResponseResult<CommentModel> Delete(Guid id);

    }
}
