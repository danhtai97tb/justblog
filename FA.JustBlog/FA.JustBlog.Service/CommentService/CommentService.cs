﻿using AutoMapper;
using FA.JustBlog.Entity.Entity;
using FA.JustBlog.Models.Models;
using FA.JustBlog.Models.Response;
using FA.JustBlog.Repository.Infrastructures;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Service.CommentService
{
    public class CommentService : ICommentService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper mapper;

        public CommentService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public ResponseResult<CommentModel> Create(CommentModel model)
        {
            try
            {
                if (model == null)
                {
                    return new ResponseResult<CommentModel>("No have data to Create a Comment!");
                }
                var entity = new Comment();
                entity.Id = new Guid();
                entity.Name = model.Name;
                entity.CommentText = model.CommentText;
                entity.CommentHeader = model.CommentHeader;
                entity.PostId = model.PostId;
                entity.CommentTime = DateTime.Now;
                entity.Email = model.Email;
                _unitOfWork.CommentRepository.Add(entity);
                _unitOfWork.SaveChanges();
                return new ResponseResult<CommentModel>() { Data = model, StatusCode = 201 };
            }
            catch (Exception ex)
            {
                return new ResponseResult<CommentModel>(ex.Message + ex.InnerException.Message);
            }

        }

        public ResponseResult<CommentModel> Create(Guid postId, string commentName, string commentEmail, string commentTitle, string commentBody)
        {
            try
            {
                var comment = new CommentModel();
                comment.Name = commentName;
                comment.CommentTime = DateTime.Now;
                comment.Email = commentEmail;
                comment.CommentHeader = commentTitle;
                comment.CommentText = commentBody;
                comment.Id = new Guid();
                comment.PostId = postId;
                var model = new Comment()
                {
                    Id = comment.Id,
                    Name = comment.Name,
                    CommentHeader = comment.CommentHeader,
                    CommentText = comment.CommentText,
                    CommentTime = comment.CommentTime,
                    PostId = comment.PostId,
                    Email = comment.Email,
                };
                _unitOfWork.CommentRepository.Add(model);
                _unitOfWork.SaveChanges();
                return new ResponseResult<CommentModel>() { Data = comment };

            }
            catch (Exception ex)
            {

                return new ResponseResult<CommentModel>(ex.Message + ex.InnerException.Message);
            }

        }

        public ResponseResult<CommentModel> Delete(CommentModel model)
        {
            try
            {
                var result = _unitOfWork.CommentRepository.Find(model.Id);
                if (result != null)
                {
                    _unitOfWork.CommentRepository.Delete(result);
                    _unitOfWork.SaveChanges();
                    return new ResponseResult<CommentModel>();
                }
                return new ResponseResult<CommentModel>($"Not found the Comment has Id = {model.Id}!");

            }
            catch (Exception ex)
            {
                return new ResponseResult<CommentModel>(ex.Message + ex.InnerException.Message);
            }
        }

        public ResponseResult<CommentModel> Delete(Guid id)
        {
            try
            {
                var result = _unitOfWork.CommentRepository.Find(id);
                if (result != null)
                {
                    _unitOfWork.CommentRepository.Delete(id);
                    _unitOfWork.SaveChanges();
                    return new ResponseResult<CommentModel>();
                }
                return new ResponseResult<CommentModel>($"Not found the Comment has Id = {id}!");

            }
            catch (Exception ex)
            {
                return new ResponseResult<CommentModel>(ex.Message + ex.InnerException.Message);
            }

        }

        public ResponseResult<CommentModel> Find(Guid commentId)
        {
            var result = _unitOfWork.CommentRepository.Find(commentId);
            var postName = _unitOfWork.PostRepository.Find(result.PostId);
            if (result != null)
            {
                var model = new CommentModel();
                model.Name = result.Name;
                model.Id = result.Id;
                model.PostTitle = postName.Title;
                model.CommentText = result.CommentText;
                model.CommentTime = result.CommentTime;
                model.CommentHeader = result.CommentHeader;
                model.Email = result.Email;
                model.PostId = result.PostId;

                return new ResponseResult<CommentModel>() { Data = model };
            }
            return new ResponseResult<CommentModel>($"Not found the Comment has Id = {commentId}!");

        }

        public ResponseResult<CommentModel> GetAll()
        {
            var result = _unitOfWork.CommentRepository.GetAll().ToList();
            List<CommentModel> models = new List<CommentModel>();
            if (result.Count() > 0)
            {
                foreach (var comment in result)
                {
                    var commentModel = new CommentModel();
                    commentModel.Id = comment.Id;
                    commentModel.CommentHeader = comment.CommentHeader;
                    commentModel.CommentText = comment.CommentText;
                    commentModel.CommentTime = comment.CommentTime;
                    var post = _unitOfWork.PostRepository.Find(comment.PostId);
                    commentModel.PostTitle = post.Title;
                    commentModel.Email = comment.Email;
                    commentModel.Name = comment.Name;
                    models.Add(commentModel);
                }
                return new ResponseResult<CommentModel>() { DataList = models };
            }
            return new ResponseResult<CommentModel>("No have any data!");

        }

        public ResponseResult<CommentModel> GetCommentsForPost(Guid postId)
        {
            try
            {
                var result = _unitOfWork.CommentRepository.GetCommentsForPost(postId);

                List<CommentModel> models = new List<CommentModel>();
                foreach (var item in result)
                {
                    var comment = new CommentModel();
                    comment.Name = item.Name;
                    comment.CommentText = item.CommentText;
                    comment.CommentTime = item.CommentTime;
                    comment.Email = item.Email;
                    comment.Id = item.Id;
                    comment.CommentHeader = item.CommentHeader;
                    models.Add(comment);
                }
                return new ResponseResult<CommentModel>() { DataList = models };
            }
            catch (Exception ex)
            {
                return new ResponseResult<CommentModel>("No have any data!");
            }

        }

        public ResponseResult<CommentModel> Update(CommentModel model)
        {
            try
            {
                var commentExsit = _unitOfWork.CommentRepository.Find(model.Id);
                if (commentExsit == null)
                {
                    return new ResponseResult<CommentModel>("Comment not existing in the database") { StatusCode = 404 };
                }

                commentExsit.Name = model.Name;
                commentExsit.CommentText = model.CommentText;
                commentExsit.CommentTime = DateTime.Now;
                commentExsit.CommentHeader = model.CommentHeader;
                commentExsit.Email = model.Email;
                commentExsit.PostId = model.PostId;
                _unitOfWork.CommentRepository.Update(commentExsit);
                _unitOfWork.SaveChanges();
                return new ResponseResult<CommentModel>() { Data = model };
                
            }
            catch (Exception ex)
            {
                return new ResponseResult<CommentModel>(ex.Message + ex.InnerException.Message);
            }
        }
    }
}



