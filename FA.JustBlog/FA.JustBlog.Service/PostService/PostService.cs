﻿using AutoMapper;
using FA.Common;
using FA.JustBlog.Entity.Entity;
using FA.JustBlog.Models.Models;
using FA.JustBlog.Models.Models.Post;
using FA.JustBlog.Models.Response;
using FA.JustBlog.Repository.Infrastructures;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Net.WebSockets;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Service.PostService
{
    public class PostService : IPostService
    {
        private readonly Repository.Infrastructures.IUnitOfWork _unitOfWork;
        private readonly IMapper mapper;
        public PostService(Repository.Infrastructures.IUnitOfWork unitOfWork , IMapper mapper)
        {
            this._unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public ResponseResult<CreatePostViewModel> Create(CreatePostViewModel request)
        {
            try
            {
                var post = new Post();
                post.Id = Guid.NewGuid();
                post.Title = request.Title;
                post.UrlSlug = SeoUrlHepler.FrientlyUrl(request.Title);
                post.PostContent = request.PostContent;
                post.PostedOn = request.PostedOn;
                post.ShortDescription = request.ShortDescription;
                post.Published = request.Published;
                post.CategoryId = request.CategoryId;
                _unitOfWork.PostRepository.Add(post);
                if (request.TagName != null)
                {
                    var listtags = request.TagName.Split(",");
                    foreach (var item in listtags)
                    {
                        if (_unitOfWork.TagRepository.GetTagByUrlSlug(item) == null)
                        {
                            var tag = new Tag();
                            tag.Id = new Guid();
                            tag.Name = item;
                            tag.UrlSlug = item;
                            tag.Description = item;
                            _unitOfWork.TagRepository.Add(tag);
                            _unitOfWork.PostTagMapRepository.Add(new PostTagMap() { PostId = post.Id, TagId = tag.Id });
                        }
                        else
                        {
                            var tags = _unitOfWork.TagRepository.GetTagByUrlSlug(item);
                            _unitOfWork.PostTagMapRepository.Add(new PostTagMap() { PostId = post.Id, TagId = tags.Id });

                        }
                    }
                }
                _unitOfWork.SaveChanges();
                return new ResponseResult<CreatePostViewModel>() { Data = request, StatusCode = 201 };
            }
            catch (Exception ex)
            {
                return new ResponseResult<CreatePostViewModel>(ex.Message + ex.InnerException.Message);
            }
        }

        public ResponseResult<PostModel> Delete(PostModel request)
        {
            throw new NotImplementedException();
        }

        public ResponseResult<PostModel> Delete(Guid id)
        {
            try
            {
                var checkModel = _unitOfWork.PostRepository.Find(id);
                if (checkModel != null)
                {
                    _unitOfWork.PostRepository.Delete(id);
                    _unitOfWork.SaveChanges();
                    return new ResponseResult<PostModel>();
                }
                return new ResponseResult<PostModel>($"Not found the Post has id = {id}");
            }
            catch (Exception ex)
            {
                return new ResponseResult<PostModel>(ex.Message + ex.InnerException.Message);
            }
        }

        public IEnumerable<PostModel> GetAll()
        {
            var posts = _unitOfWork.PostRepository.GetAll();
            List<PostModel> x = new List<PostModel>();
            foreach (var item in posts)
            {
                var y = _unitOfWork.PostRepository.GetTag(item.Id);
                var postModel = new PostModel();
                postModel.Id = item.Id;
                postModel.CategoryId = item.CategoryId;
                var cate = _unitOfWork.CategoryRepository.Find(item.CategoryId);
                postModel.CategoryName = cate.Name;
                postModel.TagList = y.Select(x => new TagModel() { Name = x.Name }).ToList();
                postModel.TagName = String.Join(",", y.Select(x => x.Name).ToList());
                postModel.Title = item.Title;
                postModel.PostContent = item.PostContent;
                postModel.ShortDescription = item.ShortDescription;
                postModel.ViewCount = item.ViewCount;
                postModel.TotalRate = item.TotalRate;
                postModel.RateCount = item.RateCount;
                postModel.UrlSlug = item.UrlSlug;
                postModel.PostedOn = item.PostedOn;
                x.Add(postModel);
            }
            return x;


        }

      

        public PostModel GetByPostId(Guid id)
        {
            var x = _unitOfWork.PostRepository.Find(id);
            var cate = _unitOfWork.CategoryRepository.Find(x.CategoryId);
            var model = new PostModel();

            model.Title = x.Title;
            model.RateCount = x.RateCount;
            model.Modified = x.Modified;
            model.PostContent = x.PostContent;
            model.ShortDescription = x.ShortDescription;
            model.PostedOn = x.PostedOn;
            model.TotalRate = x.TotalRate;
            model.Published = x.Published;
            model.UrlSlug = x.UrlSlug;
            model.ViewCount = x.ViewCount;
            model.CategoryName = cate.Name;
            model.CategoryId = x.CategoryId;
            var lstComment = _unitOfWork.CommentRepository.GetCommentsForPost(id);
            model.CommentList = lstComment.Select(x => new CommentModel() { Id = x.Id, Name = x.Name , CommentTime = x.CommentTime , CommentHeader = x.CommentHeader , CommentText = x.CommentText , Email = x.Email , PostId = x.PostId }).ToList();
            return model;
        }

        public PostModel GetCategoryName(Guid categoryId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<PostModel> GetLastestPost(int size)
        {
            return _unitOfWork.PostRepository.GetLatestPost(size).Select(x => new PostModel()
            {
                Id = x.Id,
                Title = x.Title,
                RateCount = x.RateCount,
                Modified = x.Modified,
                PostContent = x.PostContent,
                ShortDescription = x.ShortDescription,
                PostedOn = x.PostedOn,
                TotalRate = x.TotalRate,
                Published = x.Published,
                UrlSlug = x.UrlSlug,
                ViewCount = x.ViewCount,
                CategoryId = x.CategoryId,
            }).ToList();
        }

        public IEnumerable<PostModel> GetMostViewPost(int size)
        {
            return _unitOfWork.PostRepository.GetMostViewedPost(size).Select(x => new PostModel()
            {
                Id = x.Id,
                Title = x.Title,
                RateCount = x.RateCount,
                Modified = x.Modified,
                PostContent = x.PostContent,
                ShortDescription = x.ShortDescription,
                PostedOn = x.PostedOn,
                TotalRate = x.TotalRate,
                Published = x.Published,
                UrlSlug = x.UrlSlug,
                ViewCount = x.ViewCount,
                CategoryId = x.CategoryId,
            }).ToList();
        }

        public IEnumerable<PostModel> GetPostByCategory(string category)
        {
            var x = _unitOfWork.PostRepository.GetPostsByCategory(category);
            List<PostModel> model = new List<PostModel>();
            foreach (var item in x)
            {
                var postModel = new PostModel();
                postModel.Id = item.Id;
                postModel.CategoryId = item.CategoryId;
                var cate = _unitOfWork.CategoryRepository.Find(item.CategoryId);
                postModel.CategoryName = cate.Name;
                postModel.Title = item.Title;
                postModel.PostContent = item.PostContent;
                postModel.ShortDescription = item.ShortDescription;
                postModel.ViewCount = item.ViewCount;
                postModel.TotalRate = item.TotalRate;
                postModel.RateCount = item.RateCount;
                postModel.UrlSlug = item.UrlSlug;
                model.Add(postModel);
            }
            return model;
        }

        public ResponseResult<EditPostViewModel> GetPostById(Guid Id)
        {
            try
            {
                var x = _unitOfWork.PostRepository.Find(Id);
                if (x == null)
                {
                    return new ResponseResult<EditPostViewModel>($"Can't find the category that has id = {Id}");
                }

                var cate = _unitOfWork.CategoryRepository.Find(x.CategoryId);
                var model = new EditPostViewModel();
                model.Title = x.Title;
                model.PostContent = x.PostContent;
                model.ShortDescription = x.ShortDescription;
                model.Published = x.Published;
                model.UrlSlug = x.UrlSlug;
                model.CategoryId = x.CategoryId;
                var y = _unitOfWork.PostRepository.GetTag(x.Id);
                model.TagList = y.Select(x => new TagModel() { Name = x.Name }).ToList();
                model.TagName = String.Join(",", y.Select(x => x.Name).ToList());
                var lstComment = _unitOfWork.CommentRepository.GetCommentsForPost(x.Id);
                model.CommentList = lstComment.Select(x => new CommentModel()
                {
                    Id = x.Id,
                    Name = x.Name,
                    CommentTime = x.CommentTime,
                    CommentHeader = x.CommentHeader,
                    CommentText = x.CommentText
                });
                return new ResponseResult<EditPostViewModel>() { Data = model };
            }
            catch (Exception ex)
            {
                return new ResponseResult<EditPostViewModel>(ex.Message + ex.InnerException.Message);
            }

        }

        public PostModel GetPostByMonthYear(int year, int month, string urlSlug)
        {
            var model = new PostModel();
            var x = _unitOfWork.PostRepository.FindPost(year, month, urlSlug);
            model.Title = x.Title;
            model.PostedOn = x.PostedOn;
            model.PostContent = x.PostContent;
            model.ShortDescription = x.ShortDescription;
            model.ViewCount = x.ViewCount;
            model.RateCount = x.RateCount;
            model.UrlSlug = x.UrlSlug;
            model.TotalRate = x.TotalRate;
            model.Id = x.Id;
            var cate = _unitOfWork.CategoryRepository.Find(x.CategoryId);
            model.CategoryName = cate.Name;
            var y = _unitOfWork.PostRepository.GetTag(x.Id);
            foreach (var items in y)
            {
                model.TagName = items.Name;
            }
            var lstComment = _unitOfWork.CommentRepository.GetCommentsForPost(x.Id);
            model.CommentList = lstComment.Select(x => new CommentModel()
            {
                Id = x.Id,
                Name = x.Name,
                CommentTime = x.CommentTime,
                CommentHeader = x.CommentHeader,
                CommentText = x.CommentText,
                PostId = x.PostId,
                
            }).ToList();
            return model;
        }

        public IEnumerable<PostModel> GetPostByTag(string tag)
        {

            var x = _unitOfWork.PostRepository.GetPostsByTag(tag);
            List<PostModel> model = new List<PostModel>();
            foreach (var item in x)
            {
                var postModel = new PostModel();
                postModel.Id = item.Id;
                postModel.CategoryId = item.CategoryId;
                var cate = _unitOfWork.CategoryRepository.Find(item.CategoryId);
                postModel.CategoryName = cate.Name;
                var y = _unitOfWork.PostRepository.GetTag(item.Id);
                postModel.TagList = y.Select(x => new TagModel { Name = x.Name }).ToList();
                postModel.Title = item.Title;
                postModel.PostContent = item.PostContent;
                postModel.ShortDescription = item.ShortDescription;
                postModel.ViewCount = item.ViewCount;
                postModel.TotalRate = item.TotalRate;
                postModel.RateCount = item.RateCount;
                postModel.UrlSlug = item.UrlSlug;
                model.Add(postModel);
            }
            return model;
        }

        public IEnumerable<PostModel> GetPublishedPost()
        {
            return _unitOfWork.PostRepository.GetPublisedPosts().Select(x => new PostModel()
            {
                Id = x.Id,
                Title = x.Title,
                RateCount = x.RateCount,
                Modified = x.Modified,
                PostContent = x.PostContent,
                ShortDescription = x.ShortDescription,
                PostedOn = x.PostedOn,
                TotalRate = x.TotalRate,
                Published = x.Published,
                UrlSlug = x.UrlSlug,
                ViewCount = x.ViewCount,
                CategoryId = x.CategoryId,
            }).ToList();
        }

        public ResponseResult<EditPostViewModel> Update(EditPostViewModel request)
        {
            try
            {
                var postExist = _unitOfWork.PostRepository.Find(request.Id);
                if (postExist == null)
                {
                    return new ResponseResult<EditPostViewModel>("Post not existing in the database") { StatusCode = 404 };
                }
                postExist.Title = request.Title;
                postExist.ShortDescription = request.ShortDescription;
                postExist.UrlSlug = SeoUrlHepler.FrientlyUrl(request.Title);
                postExist.CategoryId = request.CategoryId;
                postExist.Published = request.Published;
                postExist.PostContent = request.PostContent;
                _unitOfWork.PostRepository.Update(postExist);


                var listTag = request.TagName.Split(",").ToList();
                var listOldTag = _unitOfWork.PostRepository.GetTag(request.Id).Select(x => x.Name).ToList();
                var delete = listOldTag.Except(listTag);
                var add = listTag.Except(listOldTag);
                if(add.Count() > 0) 
                {
                    var listAddNewTag = new List<Guid>();
                    foreach(var item in add)
                    {
                        var addTag = _unitOfWork.TagRepository.FindTagByName(item);
                        if(addTag == null)
                        {
                            var tag = new Tag();
                            tag.Id = new Guid();
                            tag.Name = item;
                            tag.UrlSlug = item;
                            tag.Description = item;
                            _unitOfWork.TagRepository.Add(tag);
                            _unitOfWork.PostTagMapRepository.Add(new PostTagMap() { PostId = request.Id, TagId = tag.Id });
                        }
                        else
                        {
                            listAddNewTag.Add(addTag.Id);
                        }
                    }
                    foreach (var item in listAddNewTag)
                    {
                        _unitOfWork.PostTagMapRepository.Add(new PostTagMap() { PostId = request.Id, TagId = item });
                    }
                }
                if(delete.Count() > 0)
                {
                    foreach(var item in delete)
                    {
                        var deleteTag = _unitOfWork.TagRepository.FindTagByName(item);
                        if (delete != null)
                        {
                            _unitOfWork.PostTagMapRepository.Delete(new PostTagMap { PostId = request.Id, TagId = deleteTag.Id });
                            deleteTag.Count--;
                            _unitOfWork.TagRepository.Update(deleteTag);
                        }
                    }
                }
                
                //
                //
                _unitOfWork.SaveChanges();
                return new ResponseResult<EditPostViewModel>() { Data = request, StatusCode = 201 };

            }
            catch (Exception ex)
            {
                return new ResponseResult<EditPostViewModel>(ex.Message + ex.InnerException.Message);
            }
        }
    }
}
