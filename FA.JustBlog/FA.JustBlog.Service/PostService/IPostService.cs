﻿using FA.Common;
using FA.JustBlog.Entity.Entity;
using FA.JustBlog.Models.Models.Post;
using FA.JustBlog.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Service.PostService
{
    public interface IPostService
    {
        public ResponseResult<CreatePostViewModel> Create(CreatePostViewModel request);
        public ResponseResult<PostModel> Delete(PostModel request);
       
        public ResponseResult<EditPostViewModel> GetPostById(Guid Id);

        public ResponseResult<EditPostViewModel> Update(EditPostViewModel request);

        public ResponseResult<PostModel> Delete(Guid id);
        public IEnumerable<PostModel> GetAll();
        public PostModel GetByPostId(Guid id);

        public IEnumerable<PostModel> GetPublishedPost();
        public IEnumerable<PostModel> GetLastestPost(int size);
        public IEnumerable<PostModel> GetMostViewPost(int size);
        public IEnumerable<PostModel> GetPostByCategory(string category);
        public IEnumerable<PostModel> GetPostByTag(string tag);
        public PostModel GetCategoryName(Guid categoryId);

        public PostModel GetPostByMonthYear(int year , int month , string urlSlug);

    }
}
