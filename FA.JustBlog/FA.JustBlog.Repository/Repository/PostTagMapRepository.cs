﻿using FA.JustBlog.Entity.Data;
using FA.JustBlog.Entity.Entity;
using FA.JustBlog.Repository.Infrastructures;
using FA.JustBlog.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Repository.Repository
{
    public class PostTagMapRepository : BaseRepository<PostTagMap>, IPostTagMapRepository
    {
        public PostTagMapRepository(JustBlogContext context) : base(context)
        {
        }

        public PostTagMap FindPostTag(Guid tagId, Guid postId)
        {
            return Context.PostTagMaps.Find(tagId, postId);
        }
    }
}
