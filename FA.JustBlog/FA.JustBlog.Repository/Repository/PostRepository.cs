﻿using FA.Common;
using FA.JustBlog.Entity.Data;
using FA.JustBlog.Entity.Entity;
using FA.JustBlog.Repository.Infrastructures;
using FA.JustBlog.Repository.IRepository;
using System.Linq.Expressions;

namespace FA.JustBlog.Repository.Repository
{
    public class PostRepository : BaseRepository<Post>, IPostRepository
    {
        public PostRepository(JustBlogContext context) : base(context)
        {
        }

        public int CountPostsForCategory(string category)
        {
            return Context.Posts.Count(x => x.Category.Name.Equals(category));
        }

        public int CountPostsForTag(string tag)
        {
            return Context.Posts.Count(x => x.PostTagMaps.Any(t => t.Tag.Name.Equals(tag)));
        }

        public Post FindPost(int year, int month, string urlSlug)
        {
            //var result = from Post in Context.Posts
            //             where Post.PostedOn.Year == year && Post.PostedOn.Month == month && Post.Title.Equals(title)
            //             select Post;
            //return (Post)result;
            return Context.Posts.FirstOrDefault(x => x.PostedOn.Year == year && x.PostedOn.Month == month && x.UrlSlug == urlSlug);
        }

       

        public IEnumerable<Post> GetHighestPosts(int size)
        {
            return Context.Posts.OrderByDescending(p => (double)(p.TotalRate / p.RateCount)).Take(size).ToList();
        }

        public IEnumerable<Post> GetLatestPost(int size)
        {
            return Context.Posts.OrderByDescending(p => p.PostedOn).Take(size).ToList();
        }

        public IEnumerable<Post> GetMostViewedPost(int size)
        {
            return Context.Posts.OrderByDescending(x => x.ViewCount).Take(size).ToList();
        }

        public IEnumerable<Post> GetPostsByCategory(string category)
        {
            return Context.Posts.Where(p => p.Category.Name.Equals(category)).ToList();
        }

        public IEnumerable<Post> GetPostsByMonth(DateTime monthYear)
        {
            return Context.Posts.Where(p => p.PostedOn.Month == monthYear.Month && p.PostedOn.Year == monthYear.Year).ToList();
        }

        public IEnumerable<Post> GetPostsByTag(string tag)
        {
            return Context.Posts.Where(p => p.PostTagMaps.Any(pt => pt.Tag.Name.Equals(tag))).ToList();
        }

        public IEnumerable<Post> GetPublisedPosts()
        {
            return Context.Posts.Where(x => x.Published == true).ToList();
        }

        public IEnumerable<Tag> GetTag(Guid id)
        {
            //var resul = from Post in Context.Posts
            //            join PostTagMap in Context.PostTagMaps on Post.Id equals PostTagMap.PostId
            //            join Tags in Context.Tags on PostTagMap.TagId equals Tags.Id
            //            where Post.Id == id
            //            select Tags;
            //return resul.ToList();
            return Context.Tags.Where(x => x.PostTagMaps.Any(x => x.PostId == id)).ToList();
        }

        public IEnumerable<Post> GetUnpublisedPosts()
        {
            return Context.Posts.Where(x => x.Published == false).ToList();
        }
    }
}