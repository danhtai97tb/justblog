﻿using FA.JustBlog.Entity.Data;
using FA.JustBlog.Entity.Entity;
using FA.JustBlog.Repository.Infrastructures;
using FA.JustBlog.Repository.IRepository;

namespace FA.JustBlog.Repository.Repository
{
    public class TagRepository : BaseRepository<Tag>, ITagRepository
    {
        public TagRepository(JustBlogContext context) : base(context)
        {
        }

        public Tag FindTagByName(string name)
        {
            return Context.Tags.FirstOrDefault(x => x.Name.Equals(name));
        }

        public IEnumerable<Tag> GetTagByPost(Guid id)
        {
            return Context.Tags.Where(x => x.PostTagMaps.Any(y => y.PostId == id)).ToList();
        }

        public Tag GetTagByUrlSlug(string urlSlug)
        {
            return Context.Tags.FirstOrDefault(x => x.UrlSlug == urlSlug);
        }
    }
}