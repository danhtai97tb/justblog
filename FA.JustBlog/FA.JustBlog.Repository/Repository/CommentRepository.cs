﻿using FA.JustBlog.Entity.Data;
using FA.JustBlog.Entity.Entity;
using FA.JustBlog.Repository.Infrastructures;
using FA.JustBlog.Repository.IRepository;

namespace FA.JustBlog.Repository.Repository
{
    public class CommentRepository : BaseRepository<Comment>, ICommentRepository
    {
        public CommentRepository(JustBlogContext context) : base(context)
        {
        }

        public void AddComment(Guid postId, string commentName, string commentEmail, string commentTitle, string commentBody)
        {
            var comment = new Comment()
            {
                PostId = postId,
                Name = commentName,
                Email = commentEmail,
                CommentHeader = commentTitle,
                CommentText = commentBody,
                CommentTime = DateTime.Now
            };
            Context.Add(comment);
            Context.SaveChanges();
        }

        public IEnumerable<Comment> GetCommentsForPost(Guid postId)
        {
            return Context.Comments.Where(p => p.PostId == postId).ToList();
        }

        public IEnumerable<Comment> GetCommentsForPost(Post post)
        {
            var result = Context.Comments.Where(m => m.PostId == post.Id).ToList();
            return result;
        }
    }
}