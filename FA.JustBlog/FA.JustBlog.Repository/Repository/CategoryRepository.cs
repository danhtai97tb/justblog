﻿using FA.JustBlog.Entity.Data;
using FA.JustBlog.Entity.Entity;
using FA.JustBlog.Repository.Infrastructures;
using FA.JustBlog.Repository.IRepository;

namespace FA.JustBlog.Repository.Repository
{
    public class CategoryRepository : BaseRepository<Category>, ICategoryRepository
    {
        public CategoryRepository(JustBlogContext context) : base(context)
        {
        }
    }
}