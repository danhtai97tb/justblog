﻿using FA.JustBlog.Entity.Entity;
using FA.JustBlog.Repository.Infrastructures;

namespace FA.JustBlog.Repository.IRepository
{
    public interface ITagRepository : IBaseRepository<Tag>
    {
        /// <summary>
        /// Get Tag by UrlSlug
        /// </summary>
        /// <param name="urlSlug"></param>
        /// <returns></returns>
        Tag GetTagByUrlSlug(string urlSlug);

        IEnumerable<Tag> GetTagByPost(Guid id);

        Tag FindTagByName(string name);
    }
}