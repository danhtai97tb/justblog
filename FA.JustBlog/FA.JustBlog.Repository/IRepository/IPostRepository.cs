﻿using FA.Common;
using FA.JustBlog.Entity.Entity;
using FA.JustBlog.Repository.Infrastructures;
using System.Linq.Expressions;

namespace FA.JustBlog.Repository.IRepository
{
    public interface IPostRepository : IBaseRepository<Post>
    {
        IEnumerable<Post> GetAll();
        /// <summary>
        /// Find Post by Year, Month and urlSlug
        /// </summary>
        /// <param name="year">Year</param>
        /// <param name="month">Month</param>
        /// <param name="title">Title</param>
        /// <returns></returns>
        Post FindPost(int year, int month, string urlSlug );

       

        /// <summary>
        ///  Get Published Post
        /// </summary>
        /// <returns></returns>
        IEnumerable<Post> GetPublisedPosts();

        /// <summary>
        /// Get UnPublished Post
        /// </summary>
        /// <returns></returns>
        IEnumerable<Post> GetUnpublisedPosts();

        /// <summary>
        ///  Get Lastest Post
        /// </summary>
        /// <param name="size">number of Post want to get</param>
        /// <returns></returns>
        IEnumerable<Post> GetLatestPost(int size);

        /// <summary>
        /// Get Post by Month
        /// </summary>
        /// <param name="monthYear">Date</param>
        /// <returns></returns>
        IEnumerable<Post> GetPostsByMonth(DateTime monthYear);

        /// <summary>
        /// Count Post By Category
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        int CountPostsForCategory(string category);

        /// <summary>
        /// Get Post By Category
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        IEnumerable<Post> GetPostsByCategory(string category);

        /// <summary>
        ///  Count Post By Tag
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        int CountPostsForTag(string tag);

        /// <summary>
        /// Get Post By Tag
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        IEnumerable<Post> GetPostsByTag(string tag);

        /// <summary>
        /// Get View Post
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        IEnumerable<Post> GetMostViewedPost(int size);

        /// <summary>
        /// Get highest Post
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        IEnumerable<Post> GetHighestPosts(int size);

        IEnumerable<Tag> GetTag(Guid id);

    }
}