﻿using FA.JustBlog.Entity.Entity;
using FA.JustBlog.Repository.Infrastructures;

namespace FA.JustBlog.Repository.IRepository
{
    public interface ICommentRepository : IBaseRepository<Comment>
    {
        /// <summary>
        /// Add Comment
        /// </summary>
        /// <param name="postId"></param>
        /// <param name="commentName"></param>
        /// <param name="commentEmail"></param>
        /// <param name="commentTitle"></param>
        /// <param name="commentBody"></param>
        void AddComment(Guid postId, string commentName, string commentEmail, string commentTitle, string commentBody);

        /// <summary>
        /// Get Comment for post with postId
        /// </summary>
        /// <param name="postId"></param>
        /// <returns></returns>
        IEnumerable<Comment> GetCommentsForPost(Guid postId);

        /// <summary>
        /// Get Comment for Post
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        IEnumerable<Comment> GetCommentsForPost(Post post);
    }
}