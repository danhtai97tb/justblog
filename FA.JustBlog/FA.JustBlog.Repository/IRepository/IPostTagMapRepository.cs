﻿using FA.JustBlog.Entity.Entity;
using FA.JustBlog.Repository.Infrastructures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Repository.IRepository
{
    public interface IPostTagMapRepository : IBaseRepository<PostTagMap>
    {
        PostTagMap FindPostTag(Guid tagId, Guid postId);
    }
}
