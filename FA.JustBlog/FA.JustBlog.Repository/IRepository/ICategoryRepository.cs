﻿using FA.JustBlog.Entity.Entity;
using FA.JustBlog.Repository.Infrastructures;

namespace FA.JustBlog.Repository.IRepository
{
    public interface ICategoryRepository : IBaseRepository<Category>
    {

    }
}