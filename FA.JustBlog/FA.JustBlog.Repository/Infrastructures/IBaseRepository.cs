﻿using System.Linq.Expressions;
using FA.Common;

namespace FA.JustBlog.Repository.Infrastructures
{
    public interface IBaseRepository<TEntity> where TEntity : class
    {
        /// <summary>
        /// Find Data with Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        TEntity Find(Guid Id);

        /// <summary>
        /// Change state of entity to added
        /// </summary>
        /// <param name="entity"></param
        void Add(TEntity entity);

        /// <summary>
        /// Change state of entity to modified
        /// </summary>
        /// <param name="entity"></param>
        void Update(TEntity entity);

        /// <summary>
        /// Change state of entity to deleted
        /// </summary>
        /// <param name="entity"></param>
        void Delete(TEntity entity);

        /// <summary>
        /// Change state of entity to deleted
        /// </summary>
        /// <param name="Id"></param>
        void Delete(Guid Id);

        /// <summary>
        /// Get all <paramref name="TEntity"></paramref> from database
        /// </summary>
        /// <returns></returns>
        IEnumerable<TEntity> GetAll();


        Task<IEnumerable<TEntity>> GetByPageAsync(Expression<Func<TEntity, bool>> condition, int size, int page);
        IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>,
           IOrderedQueryable<TEntity>> orderBy = null, string includeProperties = "");
    }
}