﻿using FA.JustBlog.Entity.Data;
using FA.JustBlog.Repository.IRepository;

namespace FA.JustBlog.Repository.Infrastructures
{
    public interface IUnitOfWork : IDisposable
    {
        public ICategoryRepository CategoryRepository { get; }
        public IPostRepository PostRepository { get; }
        public ITagRepository TagRepository { get; }

        public IPostTagMapRepository PostTagMapRepository { get; }

        public ICommentRepository CommentRepository { get; }
        public JustBlogContext JustBlogContext { get; }

        int SaveChanges();
    }
}