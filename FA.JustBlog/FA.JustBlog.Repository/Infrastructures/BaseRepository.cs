﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using FA.Common;
using FA.JustBlog.Entity.Data;
using Microsoft.EntityFrameworkCore;

namespace FA.JustBlog.Repository.Infrastructures
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
    {
        protected readonly JustBlogContext Context;
        protected DbSet<TEntity> DbSet;

        public BaseRepository(JustBlogContext context)
        {
            Context = context;
            DbSet = context.Set<TEntity>();
        }

        public void Add(TEntity entity)
        {
            if(entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            DbSet.Add(entity);
            Context.SaveChanges();
        }

        public void Delete(TEntity entity)
        {
            DbSet.Remove(entity);
            Context.SaveChanges();
        }

        public void Delete(Guid Id)
        {
            var entity = Find(Id);
            if (entity == null)
            {
                throw new ArgumentException($"{string.Join(";", Id)} not exist in the {typeof(TEntity).Name} table");
            }
            DbSet.Remove(entity);
            Context.SaveChanges();
        }

        public TEntity Find(Guid Id)
        {
            return DbSet.Find(Id);
        }

        public IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, string includeProperties = "")
        {
            IQueryable<TEntity> query = DbSet;
            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            return orderBy != null ? orderBy(query) : query;
        }

        public IEnumerable<TEntity> GetAll()
        {
            return DbSet.ToList();
        }

        public async Task<IEnumerable<TEntity>> GetByPageAsync(Expression<Func<TEntity, bool>> condition, int size, int page)
        {
            return await DbSet.Where(condition).Skip(size * (page - 1)).Take(size).ToListAsync();
        }

        public void Update(TEntity entity)
        {
            DbSet.Update(entity);
            Context.SaveChanges();
        }

      
    }
}
